/*
 * Zweck:
 * Adapter für Anzeige der Verfügbaren Fotodateien
 *
 * Author/Version/Datum:
 * mak/v1/04.6. Gerüst erstellt & Funktionalität implementiert
 * mak/v2/05.6. Klasse kommentiert
 **/
package ch.ffhs.amhs.notesatmobile.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.List;

import ch.ffhs.amhs.notesatmobile.R;
import ch.ffhs.amhs.notesatmobile.data.Foto;
import ch.ffhs.amhs.notesatmobile.data.PersistenzHelper;

/**
 * Adapter für Fotoanzeige innerhalb der Anhänge
 */
public class FotoPagerAdapter extends PagerAdapter {

    private final Context context;
    private final LayoutInflater inflater;
    private final List<Foto> fotoList;
    private final Runnable deleteDelegate;

    /**
     * Konstruktor
     * @param context Context / Activity
     * @param fotoList Liste aller Fotos
     * @param deleteDelegate Delegate-Funktion für das Löschen
     */
    public FotoPagerAdapter(@NonNull final Context context, @NonNull final List<Foto> fotoList, @NonNull final Runnable deleteDelegate) {
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.fotoList = fotoList;
        this.deleteDelegate = deleteDelegate;
    }

    /**
     * Gibt die Anzahl Elemente zurück
     * @return
     */
    @Override
    public int getCount() {
        return fotoList.size();
    }

    /**
     * Helpermethode, ob das View von dem Objekt ist
     * @param view View
     * @param object Objekt
     * @return true wenn View gleich objekt
     */
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    /**
     * Instanzierungsfunktion
     * @param container Container
     * @param position Position des Elements
     * @return Objekt (Element)
     */
    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        View itemView = inflater.inflate(R.layout.pager_item, container, false);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.fotoView);
        imageView.setImageURI(Uri.parse(fotoList.get(position).getPfad()));

        // Listener für Context Menü eines Bildes
        imageView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                new AlertDialog.Builder(context)
                        .setTitle(R.string.TitelLoeschenBestaetigen)
                        .setMessage(R.string.TitelLoeschenBestaetigen)
                        .setPositiveButton(R.string.OptionJa, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                PersistenzHelper.getInstance(context).loescheFoto(
                                        fotoList.get(position)
                                );
                                // View Updaten
                                deleteDelegate.run();
                            }
                        })
                        .setNegativeButton(R.string.OptionNein, null).show();
                return true;
            }
        });

        container.addView(itemView);
        return itemView;
    }

    /**
     * Destroy Funktion
     * @param container Container
     * @param position Position des Objektes
     * @param object Objekt
     */
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }


}
