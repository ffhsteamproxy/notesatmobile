/*
 * Zweck:
 * Transportobjekt für eine Notiz
 *
 * Author/Version/Datum:
 * mak/v1/2.5. Grundgerüst
 * hos/v2/4.5. Namensgebung
 * mak/v3/15.5. Getter für Listen hinzugefügt
 * hos/v4/22.5. Liste-Gesamt Setter weil wir vom GUI genau das bekommen auch notwendig
 * hos/v5/4.6. Konsequente Benennung, Kommentare
 */

package ch.ffhs.amhs.notesatmobile.data;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import ch.ffhs.amhs.notesatmobile.lib.NotesConstants;

/**
 * Notiz
 */
public class Notiz {

    /**
     * Persistenz-ID
     */
    private long notizId = 0;

    /**
     * Entitätsvariablen
     */
    private String titel = NotesConstants.EMPTY_STRING;
    private String details = NotesConstants.EMPTY_STRING; // details
    private Position position;
    private final List<String> stichworte = new ArrayList<>();
    private final List<Foto> fotoAnhaenge = new ArrayList<>();
    private final List<Audio> audioAnhaenge = new ArrayList<>();
    private Timestamp speicherungszeitpunkt;
    private Timestamp veraenderungszeitpunkt;

    /**
     * Konstruktur protected, sollte nur über Factory aufgerufen werden
     */
    Notiz(){}

    /**
     * Gibt die Notiz-Id zurück
     * @return
     */
    public long getNotizId() {
        return notizId;
    }

    /**
     * Setzt die Notiz-Id
     * @param notizId
     */
    public void setNotizId(final long notizId) {
        this.notizId = notizId;
    }

    /**
     * Gibt den Titel der Notiz zurück
     * @return
     */
    @NonNull
    public String getTitel() {
        return titel;
    }

    /**
     * Setzt den Titel der Notiz
     * @param titel
     */
    public void setTitel(@NonNull final String titel) {
        this.titel = titel;
    }

    /**
     * Gibt den Text einer Notiz zurück
     * @return Detailtext einer Notiz
     */
    public String getDetails() {
        return details;
    }

    /**
     * Setzt den Text einer Notiz
     * @param details Detailtext einer Notiz
     */
    public void setDetails(final String details) {

        this.details = details;
    }

    /**
     * Gibt die GPS-Position der Notiz zurück
     * @return
     */
    @Nullable
    public Position getPosition() {
        return position;
    }

    /**
     * Setzt die GPS-Position der Notiz
     * @return
     */
    public void setPosition(@NonNull final Position position) {
        this.position = position;
    }

    /**
     * Stichwort Liste auf einmal setzen - wird beim setzen der Werte gebraucht
     * @param neueStichworte
     */
    public void setStichwortList(List<String> neueStichworte){
       stichworte.clear();
        for(String stichwort: neueStichworte){
            addStichwort(stichwort);
        }
   }

    /**
     * Stichwort zur Notiz hinzufügen
     * @param stichwort Stichwort der Notiz
     */
    public void addStichwort(@NonNull final String stichwort) {
        stichworte.add(stichwort);
    }

    /**
     * Get Methode für Stichworte
     * @return Stichworte der Notiz
     */
    @NonNull
    public List<String> getStichworte() {
        return stichworte;
    }

    /**
     * Hinzufügen eines Fotos
     * @param foto Foto Instanz
     */
    public void addFoto(@NonNull final Foto foto) {
        fotoAnhaenge.add(foto);
    }

    /**
     * Get Methode für Fotos
     * @return Liste der Fotos einer Notiz
     */
    @NonNull
    public List<Foto> getFotoAnhaenge() {
        return fotoAnhaenge;
    }

    /**
     * Hinzufügen einer Audio Aufnahme
     * @param audio Audio Instanz
     */
    public void addAudio(@NonNull final Audio audio) {
        audioAnhaenge.add(audio);
    }

    /**
     * Get Methode für Audio Anhänge
     * @return Liste der Audio-Instanzen eirer Notiz
     */
    @NonNull
    public List<Audio> getAudioAnhaenge() {
        return audioAnhaenge;
    }

    /**
     * Gibt den Zeitstempel zurück, an welchem die Notiz erstellt wurde
     * @return Zeitpunkt der Erstellung
     */
    @Nullable
    public Timestamp getSpeicherungszeitpunkt() {
        return speicherungszeitpunkt;
    }

    /**
     * Setzt den Zeitstempel, an welchem die Notiz erstellt wurde
     * @param speicherungszeitpunkt Zeitpunkt der Erstellung
     */
    void setSpeicherungszeitpunkt(@NonNull Timestamp speicherungszeitpunkt) {
        this.speicherungszeitpunkt = speicherungszeitpunkt;
    }

    /**
     * Gibt den Zeitstempel zurück, an welchem die Notiz zuletzt geändert wurde.
     * @return aenderungszeitpunkt einer Aufgabe
     */
    @Nullable
    public Timestamp getVeraenderungszeitpunkt() {
        return veraenderungszeitpunkt;
    }

    /**
     * Setzt den Zeitstempel, an welchem die Notiz zuletzt verändert wurde
     * @param veraenderungszeitpunkt Zeitpunkt der Änderung
     */
    void setBearbeitetZeitpunkt(@NonNull Timestamp veraenderungszeitpunkt) {
        this.veraenderungszeitpunkt = veraenderungszeitpunkt;
    }

    /**
     * Ort in der Nähe der Position, wenn vorhanden und gesetzt
     * @return Ortsname
     */
    public String getOrt(){
        if (getPosition() != null)
            return getPosition().getNaeheOderKoordinaten();
        return NotesConstants.EMPTY_STRING;
    }
}
