/*
 * Zweck:
 * Hilfsklasse für den Zugriff auf SQLite,
 * diese Klasse bietet den Zugriff auf die Datenbank als Singleton an
 *
 * Author/Version/Datum:
 * hos/v1/13.5. Grundgerüst
 */

package ch.ffhs.amhs.notesatmobile.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Datenbankanbindung der Applikation
 */
public class NotizDatenbank extends SQLiteOpenHelper {

    /**
     * Datenbankinformationen
     */
    private static final String DB_NAME = "notesatmobile.db";
    private static final int DB_VERSION = 1;

    /**
     * Singleton Instanz und Thread-Safety
     */
    private static NotizDatenbank instance;
    private static final Object lock = "";


    /**
     * Singleton Zugriff
     * @param context Applikationskontext
     * @return Singleton-Instanz
     */
    public static NotizDatenbank getInstance(Context context){
        // Thread-Safety
        if (instance == null){
            synchronized (lock){
                // Wenn noch nicht erstellt, Singleton im Kontext erstellen
                if (instance == null && context != null){
                    instance = new NotizDatenbank(context.getApplicationContext());
                }
            }
        }
        return instance;
    }

    /**
     * Konstruktur
     * @param context Applikationskontext
     */
    private NotizDatenbank(Context context){
        super(context, DB_NAME, null, DB_VERSION);
    }

    /**
     * Datenbank bei der Installation erstellen
     * @param db Datenbank Instanz
     */
    @Override
    public void onCreate(SQLiteDatabase db){

        // Notiz Tabelle erstellen
        db.execSQL(NotizTabelle.SQL_CREATE);

        // Stichwort Tabelle erstellen
        db.execSQL(StichwortTabelle.SQL_CREATE);

        // Tabellen für Anhänge erstellen
        db.execSQL(AudioTabelle.SQL_CREATE);
        db.execSQL(FotoTabelle.SQL_CREATE);

        // View für Cursor erstellen
        db.execSQL(NotizView.SQL_CREATE);

    }

    /**
     * Upgrade auf eine neue Version, wird von uns (aktuell) nicht unterstützt
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        // not supported
    }
}
