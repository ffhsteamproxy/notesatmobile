/*
 * Zweck:
 * Hilfsmethoden für Input, Output auf den Oberflächen
 *
 * Author/Version/Datum:
 * hos/v1/21.5. Stichworte Komma-List
 * hos/v2/25.5. Notiz aus Bundle holen, in Bundle übergeben
 * hos/v3/26.5. Erweiterung der Notiz-Bundle Methoden
 * hos/v4/5.6. Tags für die Anzeige alphabetisch sortieren
 */

package ch.ffhs.amhs.notesatmobile.ui;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import ch.ffhs.amhs.notesatmobile.data.Notiz;
import ch.ffhs.amhs.notesatmobile.data.NotizHandler;
import ch.ffhs.amhs.notesatmobile.data.PersistenzHelper;


/**
 * Hilfsklasse für die Text-Anzeige bei z.B. der Liste von Stichworten
 */
public class AnzeigeHelper {

    /** Liste-Delimiter */
    private final static String LISTE_DELIMTER = " "; // Stichworte sind Leerzeichen getrennt

    /** Zugriffsmethode für die Notiz im Bundle */
    private static final String NOTIZ_ID_BUNDLE_KEY = "NotizId";

    /**
     * Text für die Anzeige der Stichworte
     * @param stichworte Stichworte aus der Notiz
     * @return Text für die Anzeige
     */
    public static String anzeigeFuerStichworte(final List<String> stichworte){
        Collections.sort(stichworte); // Stichworte immer sortiert
        return TextUtils.join(LISTE_DELIMTER, stichworte);
    }

    /**
     * Input einer Stichwortliste über ein Textfeld
     * @param text Text in der entsprechenden Formatierung
     * @return Stichwortliste für die Notiz
     */
    public static List<String> listeFuerStichwortEingabe(String text){
        return Arrays.asList(text.split(LISTE_DELIMTER));
    }

    /**
     * Hilfsmethode: Notizdaten aus den Informationen des Bundles laden
     * @param bundle Aus dem Intent übergebenes Bundle
     */
    public static Notiz getNotizFromBundle(Bundle bundle, Context context){

        if (bundle != null){
            // ID aus dem Bundle holen
            Long notizId = bundle.getLong(NOTIZ_ID_BUNDLE_KEY);

            // Proxy erstellen
            Notiz proxy = NotizHandler.erstelleNotizProxy(notizId);

            // Notiz aus dem Speicher laden
           return PersistenzHelper.getInstance(context).ladeNotiz(proxy);
        }
        // TODO Fehler

        return null;
    }

    /**
     * Hilfsmethode: Wurde dem Bundle eine Notiz übergeben?
     * @param bundle Bundle mit den Extras
     * @return true, wenn eine Notiz übergeben wurde, sonst false
     */
    public static boolean hasNotizInBundle(Bundle bundle){
        return bundle != null && bundle.getLong(NOTIZ_ID_BUNDLE_KEY) > 0;
    }

    /**
     * Notiz für die Übergabe an eine andere Activity and das Bundle übergeben
     * @param notiz Notiz die übergeben werden soll
     * @param bundle Bundle in das die Notiz übergeben wird
     */
    public static void addNotizToBundle(Notiz notiz, Bundle bundle){
        addNotizToBundle(notiz.getNotizId(), bundle);
    }

    /**
     * Notiz-ID für die Übergabe an eine andere Activity and das Bundle übergeben
     * @param notizId Notiz-ID die übergeben werden soll
     * @param bundle Bundle in das die Notiz übergeben wird
     */

    public static void addNotizToBundle(Long notizId, Bundle bundle){
        bundle.putLong(NOTIZ_ID_BUNDLE_KEY, notizId);
    }
}
