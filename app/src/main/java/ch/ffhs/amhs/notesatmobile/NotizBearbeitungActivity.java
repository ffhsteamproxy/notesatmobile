/*
 * Zweck:
 * Anzeige für eine einzelne Notiz ohne Bearbeitungsmöglichkeiten
 * <p>
 * Author/Version/Datum:
 * hos/v1/24.4. Grundgerüst
 * hos/v2/4.5. Namensgebung
 * hos/v3/22.5. Neu, Anzeige, Werte setzen
 * hos/v4/26.5. Zurück Button
 * hos/v7/31.5. Permission Check für GPS und Location Service einbinden
 * mak/v8/4.6. Validierung der Notiz
 * hos/v9/5.6. Dirty Check und Rückfrage für Anlagen und Zurücxk
 */

package ch.ffhs.amhs.notesatmobile;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import ch.ffhs.amhs.notesatmobile.data.Notiz;
import ch.ffhs.amhs.notesatmobile.data.NotizHandler;
import ch.ffhs.amhs.notesatmobile.data.PersistenzHelper;
import ch.ffhs.amhs.notesatmobile.data.Position;
import ch.ffhs.amhs.notesatmobile.lib.BerechtigungenCheck;
import ch.ffhs.amhs.notesatmobile.lib.NotesConstants;
import ch.ffhs.amhs.notesatmobile.lib.NotizValidator;
import ch.ffhs.amhs.notesatmobile.services.LocationService;
import ch.ffhs.amhs.notesatmobile.ui.AnzeigeHelper;

/**
 * Activity für das Erstellen oder Bearbeiten einer Notiz
 */
public class NotizBearbeitungActivity extends AppCompatActivity {

    /**
     * Notizdaten für die Anzeige
     */
    private Notiz notiz;
    /**
     * Hilfsvariable, ob neue Notiz oder von der Datenbank geladen
     */
    private boolean istNeueNotiz = true;

    /**
     * GUI Elemente
     */
    private TextView itemTitel;
    private TextView itemNotiz;
    private TextView itemStichworte;
    private TextView itemToolbarTitel;

    /**
     * Service Variablen
     */
    private LocationService.LocationServiceBinder locationServiceBinder;
    private Handler locationCallbackHandler;
    private ServiceConnection locationServiceConnection;

    /**
     * Runnable Klasse für das Publishen des Ergebnis der Abfrage
     */
    public class LocationRunnable implements Runnable {
        public Position ergebnisPosition;

        public void run() {

            // Setzt das Ergebnis der GPS Abfrage in die Aufgabe
            notiz.setPosition(ergebnisPosition);
        }
    }

    /**
     * On Create einer Activity,
     * Fenster öffnen und initialisieren
     *
     * @param savedInstanceState Bundle mit übergebenen Daten
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Öffnen des Fensters
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notiz_bearbeitung);

        // Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.mainToolbar);
        setSupportActionBar(toolbar);

        // Items setzen
        setzeFelder();

        // Daten laden und anzeigen
        setNotizFromBundle(getIntent().getExtras());
        zeigeNotiz();

        // GPS Daten Service nur starten, falls es eine neue Notiz ist
        if (istNeueNotiz) {
            // Berechtigungen prüfen
            startLocationServiceAndConnect();
        }
    }

    /**
     * Hilfsmethode: Setzt die Items
     */
    private void setzeFelder() {
        itemTitel = (TextView) findViewById(R.id.FeldTitelEditId);
        itemNotiz = (TextView) findViewById(R.id.FeldNotizEditId);
        itemStichworte = (TextView) findViewById(R.id.FeldStichworteEditId);
        itemToolbarTitel = (TextView) findViewById(R.id.ToolbarBearbeitenTItel);
    }

    /**
     * Hilfsmethode: Alle Daten der Notiz auf dem GUI anzeigen
     */
    private void zeigeNotiz() {

        // Titel
        if (istNeueNotiz) {
            itemToolbarTitel.setText(R.string.TitelNeueNotiz);
        } else {
            itemToolbarTitel.setText(R.string.TitelNotizBearbeiten);
        }

        // Anzeige der Notiz (Text-)Daten
        itemTitel.setText(notiz.getTitel());
        itemNotiz.setText(notiz.getDetails());

        // Stichworte
        final String stichworteText = AnzeigeHelper.anzeigeFuerStichworte(notiz.getStichworte());
        itemStichworte.setText(stichworteText);
    }

    /**
     * Hilfsmethode: Alle Daten auf der Notiz setzen
     */
    private void setzeNotizDaten() {

        // Schreiben der Notiz (Text-)Daten
        notiz.setTitel(itemTitel.getText().toString());
        notiz.setDetails(itemNotiz.getText().toString());

        // Stichworte
        final String stichwortText = itemStichworte.getText().toString();
        final List<String> stichwortListe = AnzeigeHelper.listeFuerStichwortEingabe(stichwortText);
        notiz.setStichwortList(stichwortListe);
    }

    /**
     * Hilfsmethode: Notizdaten aus den Informationen des Bundles laden.
     * Wird dem Bundle keine Notiz mitgegeben, so wird eine neue erstellt.
     *
     * @param bundle Aus dem Intent übergebenes Bundle
     */
    private void setNotizFromBundle(Bundle bundle) {

        if (AnzeigeHelper.hasNotizInBundle(bundle)) { // und gesetzter ID Wert
            notiz = AnzeigeHelper.getNotizFromBundle(bundle, this);
            istNeueNotiz = false;
        } else {
            notiz = NotizHandler.erstelleNotiz();
        }
    }

    /**
     * Zugriff auf Speichern vom GUI aus
     *
     * @param view von GUI übergebene View
     */
    public void onNotizSpeichern(View view) {

        // Daten setzen und speichern
        setzeNotizDaten();

        // Validierung
        if (validiereNotizUndZeigeMeldung()) {
            // Validierung in Ordnung
            PersistenzHelper.getInstance(this).speichereNotiz(notiz);
            // Notiz übergeben und View schliessen
            final Bundle bundle = new Bundle();
            AnzeigeHelper.addNotizToBundle(notiz, bundle);

            // Intent erstellen und Activity staretn
            final Intent intent = new Intent(this, NotizAnzeigeActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);

            finish();
        }
    }

    /**
     * Hilfsmethode Meldung des Notizvalidators anzeigen
     *
     * @return true, wenn die Validierung korrekt läuft
     */
    private boolean validiereNotizUndZeigeMeldung() {

        // Validierung
        final NotizValidator validator = new NotizValidator(this);
        final boolean istValid = validator.validateNotiz(notiz);
        if (!istValid) {

            // Validierung fehlgeschlagen
            final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setTitle(R.string.Fehler);
            alertDialog.setMessage(validator.getLastError());
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getResources().getString(R.string.Ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        }
        return istValid;
    }

    /**
     * Hilfsmethode: wurden Werte in der Notiz angepasst?
     *
     * @return true, wenn es Änderungen hat, sonst false
     */
    private boolean isDirty() {

        // Stichworte Vergleich auf Basis der Liste sortiert
        List<String> itemListe, notizListe;
        itemListe = AnzeigeHelper.listeFuerStichwortEingabe(itemStichworte.getText().toString());
        Collections.sort(itemListe);
        notizListe = notiz.getStichworte();
        Collections.sort(itemListe);

        // Vergleich aller Werte
        return (!(itemTitel.getText().toString().equals(notiz.getTitel()) &&
                itemNotiz.getText().toString().equals(notiz.getDetails()) &&
                itemListe.equals(notizListe))) &&
                    (!(itemTitel.getText().toString().isEmpty() &&
                     itemNotiz.getText().toString().isEmpty() &&
                            itemStichworte.getText().toString().isEmpty()));
    }

    /**
     * In der Maske zurück navigieren
     *
     * @param view aus dem GUI übergebene View
     */
    public void onZurueckNavigieren(View view) {
        if (isDirty()) {
            // Nachfrage, wenn noch Änderungen vorhanden sind.
            new AlertDialog.Builder(this)
                    .setTitle(R.string.TitelNotizBearbeitenZurueck)
                    .setMessage(R.string.MessageNotizBearbeitenZurueck)
                    .setPositiveButton(R.string.OptionJa, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish(); // Die Anzeige kann beendet werden
                        }
                    })
                    .setNegativeButton(R.string.OptionNein, null)
                    .show();
        } else {
            // Ist nichts geändert, darf das Fenster ungefragt geschlossen werden
            finish(); // Die Anzeige kann ohne weitere Infos beendet werden
        }
    }

    public void onAnlagen(final View view) {

        // Anlagen dürfen nur hinzu gefügt werden, wenn die Notiz gespeichert ist
        if (isDirty()) {
            // Nachfrage, wenn noch Änderungen vorhanden sind.
            new AlertDialog.Builder(this)
                    .setTitle(R.string.TitelNotizSpeichernFuerAnlagen)
                    .setMessage(R.string.MessageNotizSpeichernFuerAnlagen)
                    .setPositiveButton(R.string.OptionJa, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Daten setzen, validieren und speichern
                            setzeNotizDaten();
                            if (validiereNotizUndZeigeMeldung()) {
                                PersistenzHelper.getInstance(getParent()).speichereNotiz(notiz);
                                startAnlagenActivity();
                            }

                        }
                    })
                    .setNegativeButton(R.string.OptionNein, null).show();
        } else {
            // Ist nichts geändert, dürfen Anlagen geöffnet werden
            startAnlagenActivity();
        }
    }

    /**
     * Hilfsmethode zum Starten der Anlagen (ohne Rückfrage)
     */
    private void startAnlagenActivity() {

        // Bundle erstellen
        final Bundle bundle = new Bundle();
        AnzeigeHelper.addNotizToBundle(notiz, bundle);
        bundle.putBoolean(AnhangActivity.FLAG_EDIT_MODE, true);

        // Intent erstellen und Activity staretn
        final Intent intent = new Intent(this, AnhangActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    /**
     * Mit dem Location Service verbinden und diesen Starten
     */
    private void startLocationServiceAndConnect() {

        // Wenn die App berechtigt ist, wird der der Dienst gestartet, ansonsten nicht
        if (BerechtigungenCheck.isBerechtigtFuerGPS(this)) {

            /*
             * Verbindungsklasse,
             * nur erstellt, wenn die Berechtigungen da sind
             */
            locationServiceConnection = new ServiceConnection() {
                /**
                 * Methodenaufruf beim Verbinden des Dienstes
                 * @param componentName
                 * @param iBinder
                 */
                @Override
                public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                    locationCallbackHandler = new Handler();
                    locationServiceBinder = (LocationService.LocationServiceBinder) iBinder;
                    locationServiceBinder.setzeCallbackHandler(locationCallbackHandler);
                    locationServiceBinder.setzeRunnable(new LocationRunnable());
                }

                /**
                 * Methodenaufruf beim schliessen der Verbindung
                 * @param componentName
                 */
                @Override
                public void onServiceDisconnected(ComponentName componentName) {

                }
            };

            // Service starten
            Log.d(NotesConstants.DEBUG_LOG_TAG, getString(R.string.MeldungStartLocationService));
            final Intent locationServiceIntent = new Intent(this, LocationService.class);
            bindService(locationServiceIntent, locationServiceConnection, Context.BIND_AUTO_CREATE);

        }
    }

    /**
     * Den Location Service Beenden, falls er läuft
     */
    private void stopLocationServiceAndDisconnect() {

        // Nur wenn der Service auch läuft
        if (locationServiceConnection != null) {
            Log.d(NotesConstants.DEBUG_LOG_TAG, getString(R.string.MeldungDisconnectLocationService));

            // sauber aufräumen
            locationCallbackHandler.removeCallbacksAndMessages(null);
            unbindService(locationServiceConnection);
            stopService(new Intent(this, LocationService.class));
        }
    }

    /**
     * beim Schliessen der Activity ...
     */
    @Override
    protected void onDestroy() {
        stopLocationServiceAndDisconnect();
        super.onDestroy();
    }
}

