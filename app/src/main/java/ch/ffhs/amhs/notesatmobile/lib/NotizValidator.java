/*
 * Zweck:
 * Notiz Validator Klasse
 *
 * Author/Version/Datum:
 * mak/v1/04.6. Grundgerüst
 */
package ch.ffhs.amhs.notesatmobile.lib;

import android.content.Context;
import android.support.annotation.NonNull;

import ch.ffhs.amhs.notesatmobile.R;
import ch.ffhs.amhs.notesatmobile.data.Notiz;

/**
 * Hilfsklasse, um den Inhalt von Notizen zu validieren.
 */
public class NotizValidator {

    private String lastError = "";
    private final Context context;

    /**
     * Konstruktor
     * @param context Kontext des Aufrufes
     */
    public NotizValidator(@NonNull final Context context) {
        this.context = context;
    }

    /**
     * Hilfsmethode, um Notizen auf Mussfelder zu Validieren
     * @param notiz
     * @return
     */
    public boolean validateNotiz(@NonNull final Notiz notiz) {
        lastError = "";
        boolean result = true;

        // Titel testen
        result = isTitelValid(notiz.getTitel());

        if (!result) {
            // Fehler, wenn kein Titel gesetzt wurde
            lastError = context.getResources().getString(R.string.TitelNichtGesetzt);
            return false;
        }

        // Details testen
        result = isDetailValid(notiz.getDetails());

        if (!result) {
            // Fehler, wenn kein Detailtext gesetzt wurde
            lastError = context.getResources().getString(R.string.DetailsNichtGesetzt);
            return false;
        }

        // Alles i.O.
        return true;
    }

    /**
     * Prüft, ob der Titel einer Notiz nicht leer ist
     * @param titel Titel der Notiz
     * @return true, wenn Valid, false wenn nicht
     */
    private boolean isTitelValid(@NonNull final String titel) {
        return !titel.isEmpty();
    }

    /**
     * Prüft, ob der Detailtext einer Notiz nicht leer ist
     * @param detail Detailtext der Notiz
     * @return true, wenn valid, false wenn nicht
     */
    private boolean isDetailValid(@NonNull final String detail) {
        return !detail.isEmpty();
    }

    /**
     * Gibt den letzten Fehler zurück.
     * @return
     */
    @NonNull
    public String getLastError() {
        return lastError;
    }
}
