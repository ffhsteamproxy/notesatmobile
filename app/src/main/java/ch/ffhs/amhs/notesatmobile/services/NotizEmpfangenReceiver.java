/*
 * Zweck:
 * Service, um die Notiz zu versenden
 *
 * Author/Version/Datum:
 * hos/v1/5.6. Implementierung
 */
package ch.ffhs.amhs.notesatmobile.services;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import ch.ffhs.amhs.notesatmobile.NotizenUebersichtActivity;
import ch.ffhs.amhs.notesatmobile.R;
import ch.ffhs.amhs.notesatmobile.data.Notiz;
import ch.ffhs.amhs.notesatmobile.data.PersistenzHelper;
import ch.ffhs.amhs.notesatmobile.lib.NotesConstants;
import ch.ffhs.amhs.notesatmobile.lib.SMSParser;
import ch.ffhs.amhs.notesatmobile.ui.AnzeigeHelper;

/**
 * Broadcast Receiver zum Empfangen der SMS
 */
public class NotizEmpfangenReceiver extends BroadcastReceiver {

    /**
     * Konstanten
     */
    private static final String PDU_DESCRIPTOR = "pdus";
    private static final String BASE_PACKAGE = "ch.ffhs.amhs.notesatmobile";

    /**
     * Methode zum Erhalten einer Nachricht mit einer Notiz
     *
     * @param context
     * @param intent
     */
    @Override
    public final void onReceive(final Context context, final Intent intent) {
        final Bundle bundle = intent.getExtras();
        if (bundle != null) {
            verarbeiteSMS(context, bundle);
        }
    }

    /**
     * "
     * SMS Text auslesen und Applikation starten
     *
     * @param context Context der Nachricht
     * @param bundle  Daten der Nachricht
     */
    private void verarbeiteSMS(final Context context, final Bundle bundle) {
        // Daten aus dem Intent holen

        // SMS Auslesen
        final Object[] pdUnits = (Object[]) bundle.get(PDU_DESCRIPTOR);
        for (Object pduAsObject : pdUnits) {
            // SMS Nachricht aus dem Intent
            final SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pduAsObject);
            if (smsMessage != null) {
                final String smsText = smsMessage.getMessageBody();
                if (smsText != null && smsText.startsWith(SMSParser.TRANSFER_KEY)) {
                    final String mobilNummer = smsMessage.getDisplayOriginatingAddress();
                    starteNotesAtMobile(context, smsText, mobilNummer);
                }
            }
        }
    }

    private void starteNotesAtMobile(Context context, String smsText, String mobilNummer) {

        Log.d(NotesConstants.DEBUG_LOG_TAG, context.getString(R.string.MeldungSMSEmpfangen, mobilNummer));

        // Notiz auslesen und speichern
        final Notiz notiz = SMSParser.parseNotiz(smsText);
        if (notiz != null) {
            PersistenzHelper.getInstance(context).speichereNotiz(notiz);
            Log.d(NotesConstants.DEBUG_LOG_TAG, context.getString(R.string.MeldungNotizGespeichert, notiz.getTitel()));
        }

        final Bundle bundle = new Bundle();
        AnzeigeHelper.addNotizToBundle(notiz, bundle);

        // Applikation starten
        final Intent activityIntent = new Intent(context, NotizenUebersichtActivity.class);
        activityIntent.putExtras(bundle);
        activityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Aktuell offene Activity finden (leider nur, wenn die App grad oben ist)
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        ComponentName cn = am.getRunningTasks(1).get(0).topActivity;

        if (cn.getPackageName().equals(BASE_PACKAGE)) {
            Log.d(NotesConstants.DEBUG_LOG_TAG, cn.getClassName());
            if (cn.getClassName().equals(NotizenUebersichtActivity.class.getName())) {
                // Übersicht aktualisieren
                context.startActivity(activityIntent);
            } else {
                Toast.makeText(context, context.getString(R.string.MeldungSMSEmpfangen, mobilNummer), Toast.LENGTH_SHORT).show();
            }
        } else { // andere Activity, starte Screen
            context.startActivity(activityIntent);
        }
    }
}
