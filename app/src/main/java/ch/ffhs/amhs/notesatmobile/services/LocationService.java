/*
 * Zweck:
 * Service zum Empfangen der Location
 *
 * Author/Version/Datum:
 * mak/v1/13.5. Grundgerüst
 * hos/v2/31.5. Service-Gerüst und Aufruf mit Rückgabe einer Dummy Position
 * hos/v3/31.5. Einbinden der Google API's
 * host/v4/2.6. Anpassungen & Ort abrufen
 *
 * TODO, Fehler-Ausgabe ... Last Known Location Ausgabe, Kein Netzwerk nicht mit vollem Fehler ausgeben
 */

package ch.ffhs.amhs.notesatmobile.services;

import android.app.Service;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import ch.ffhs.amhs.notesatmobile.NotizBearbeitungActivity;
import ch.ffhs.amhs.notesatmobile.R;
import ch.ffhs.amhs.notesatmobile.data.Position;
import ch.ffhs.amhs.notesatmobile.lib.BerechtigungenCheck;
import ch.ffhs.amhs.notesatmobile.lib.NotesConstants;

/**
 * Service zum Auslesen der GPS Daten
 */
public class LocationService extends Service implements LocationListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    /**
     * Google API Parameter
     */
    private GoogleApiClient googleApiClient;
    private final static int MIN_INTERVALL_GPS_ABFRRAGE_IN_MS = 1000; // GPS Abfrage Intervall kann gross sein, Erfassen einer Notiz dauert
    private final static int MAX_INTERVALL_GPS_ABFRRAGE_IN_MS = 5000; // GPS Abfrage Intervall kann gross sein, Erfassen einer Notiz dauert
    private final static int NUMBER_OF_ADDRESSES = 10; //Anzahl Suchtreffer pro Ortsanfrage, > 1 um möglichst einen Ortsnamen zu haben
    private final static String ORTSNAME_DELIMITER = ", ";

    /**
     * Service Variablen
     */
    private final IBinder binder = new LocationServiceBinder();
    private Handler callbackHandler;
    private NotizBearbeitungActivity.LocationRunnable runnable;

    /**
     * Service Binder Klasse mit der Funktionalität
     */
    public class LocationServiceBinder extends Binder {

        /**
         * Callback für die Resultat-Rückgabe setezn
         *
         * @param rHandler übergebener Callback Handler
         */
        public void setzeCallbackHandler(Handler rHandler) {
            callbackHandler = rHandler;
        }

        /**
         * Runnable für das Abfüllen des Resultats setzen
         *
         * @param rRunnable übergebenes Runnable für den Callback
         */
        public void setzeRunnable(final NotizBearbeitungActivity.LocationRunnable rRunnable) {
            runnable = rRunnable;
        }
    }

    /**
     * Beim erstellen des Services
     */
    @Override
    public void onCreate() {
        startGooglePlayServices();
    }

    /**
     * Hilfsmethode: Starten der Ortungsdienste
     */
    private void startGooglePlayServices() {

        if (!isGooglePlayServiceAvailable()) {
            // TODO, sauber mit Fehler beenden oder Alternative
        }

        // Google API Client erstellen
        googleApiClient = new GoogleApiClient.Builder(LocationService.this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(LocationService.this)
                .addOnConnectionFailedListener(LocationService.this).build();

        if (!googleApiClient.isConnected()) {
            Log.d(NotesConstants.DEBUG_LOG_TAG, getString(R.string.MeldungGPSStart));
            // Google API verbinden
            googleApiClient.connect();

        }
    }

    /**
     * Hilfsmethode zum Stppen des Services
     */
    private void stopGooglePlayServices() {
        googleApiClient.disconnect();
    }

    /**
     * Service verbinden
     */
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    /**
     * Service trennen
     */
    @Override
    public boolean onUnbind(Intent intent) {

        // Verbindung gleich wieder schliessen
        stopGooglePlayServices();
        callbackHandler = null;

        return super.onUnbind(intent);
    }


    /**
     * Wenn die Verbindung zum Google Play Service misslingt
     *
     * @param arg0
     */
    @Override
    public void onConnectionFailed(ConnectionResult arg0) {
        // Wenn die API Verbindung unterbrochen wird, ist uns das egal, dann haben wir halt keine Location
        Log.d(NotesConstants.ERROR_LOG_TAG, getBaseContext().getString(R.string.MeldungGPSNotConnected));

    }

    /**
     * Wen der Google Play Service verbunden ist
     *
     * @param arg0 übergebene Methode
     */
    @Override
    public void onConnected(Bundle arg0) {

        // Request für API
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(MAX_INTERVALL_GPS_ABFRRAGE_IN_MS);
        locationRequest.setFastestInterval(MIN_INTERVALL_GPS_ABFRRAGE_IN_MS);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        // Der Check geschah schon vorher
        if (BerechtigungenCheck.isBerechtigtFuerGPS(this)) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    googleApiClient, locationRequest, this);
        }
    }

    /**
     * Wenn die Verbindung zum Google Play Service unterbrochen wird
     *
     * @param arg0
     */
    @Override
    public void onConnectionSuspended(int arg0) {
        // Wenn die API Verbindung unterbrochen wird, ist uns das egal, dann haben wir halt keine Location
        Log.d(NotesConstants.DEBUG_LOG_TAG, getBaseContext().getString(R.string.MeldungGPSSuspended));
    }

    /**
     * Meldung eines neuen Orts über den Google Play Service
     *
     * @param location Location des neuen Orts
     */
    @Override
    public void onLocationChanged(Location location) {

        // Abfragen der Postionsdaten
        double longitude = location.getLongitude();
        double latitude = location.getLatitude();

        final String gefundenMeldung = Double.toString(latitude) + "," + Double.toString(longitude);
        Log.d(NotesConstants.DEBUG_LOG_TAG, getBaseContext().getString(R.string.MeldungGpsKoordinatenGefunden, new Object []{ gefundenMeldung }));

        // Adresse suchen
        final String naehe = findeOrtsnameFuer(location);

        // Position erstellen
        runnable.ergebnisPosition = new Position(longitude, latitude, naehe);

        // Resultat zurückgeben
        callbackHandler.post(runnable);
        // Sobald ein Resultat da ist, wird die Verbindung beendet um Strom zu sparen
        googleApiClient.disconnect();
    }

    /**
     * Abfrage eines möglichst idealen Ortsnamens der GPS Daten für die Anzeige auf dem GUI
     *
     * @param location Location mit den GPS Paametern
     * @return Ortsname oder NULL
     */
    private String findeOrtsnameFuer(Location location) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        Log.d(NotesConstants.DEBUG_LOG_TAG, getBaseContext().getString(R.string.MeldungAdressenSuchen));
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), NUMBER_OF_ADDRESSES);
            if(addresses.size() > 0) {
                Log.d(NotesConstants.DEBUG_LOG_TAG, getBaseContext().getString(R.string.MeldungAdressenGefunden));
                for (Address address : addresses) {
                    if (address.getLocality() != null) { // wenn es einen Ort gibt, diesen zurückgeben
                        return address.getLocality() + ORTSNAME_DELIMITER + address.getCountryName();
                    }
                }
                return addresses.get(0).getCountryName();
            }
        } catch (IOException ioException) {
            // Netzwerk oder IO Probleme
            Log.e(NotesConstants.ERROR_LOG_TAG, getBaseContext().getString(R.string.FehlerKeinNetzwerkFuerOrtsname));
            Log.e(NotesConstants.ERROR_LOG_TAG, getBaseContext().getString(R.string.MeldungDetail, new Object[]{ ioException.getLocalizedMessage()}));
        } catch (IllegalArgumentException illegalArgumentException) {
            // Catch invalid latitude or longitude values.
            Log.e(NotesConstants.ERROR_LOG_TAG, getBaseContext().getString(R.string.MeldungGSFalscheArgumente), illegalArgumentException); // Wenn hier ein Fehler kommt, dann raus mit, das sollte nicht sein
        }
        return null;
    }

    /**
     * Hilfemethode für Debugging auf dem Mobiltelefon,
     * theoretisch muss dieser Dienst nicht unterstützt werden, wird aber nicht unterstützt
     *
     * @return ob Google Play auf dsem Mobiltelefon verfügbar ist
     */
    private boolean isGooglePlayServiceAvailable() {
        int errorCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this); // nur für den Check
        if (errorCode != ConnectionResult.SUCCESS) {
            Log.d(NotesConstants.DEBUG_LOG_TAG, getBaseContext().getString(R.string.MeldungGPSNichtVerfuegbar));
            return false;
        }
        return true;
    }

}

