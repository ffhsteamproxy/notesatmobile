/*
 * Zweck:
 * Konstantenklasse für die Tabelle Audio
 *
 * Author/Version/Datum:
 * mak/v1/13.5. Grundgerüst
 * hos/v2/13.5. Namensgebung/Name hinzugefügt
 * mak/v3/15.5. FotoId in AudioId umbenennt
 */

package ch.ffhs.amhs.notesatmobile.db;


/**
 * Interface Klasse für die Spalten der Tabelle Audio
 */
public interface IAudioSpalten {

    /**
     * Datenfelder
     */
    String AUDIO_ID = "_id";
    String NOTIZ_ID = "NotizId";
    String NAME = "Name";
    String PFAD = "Pfad";
}
