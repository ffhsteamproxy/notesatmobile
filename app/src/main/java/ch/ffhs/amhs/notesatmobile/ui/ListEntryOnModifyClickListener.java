/*
 * Zweck:
 * Listener für Klick-Aktionen auf den Bearbeiten-Button eines Entries
 *
 * Author/Version/Datum:
 * mak/v1/28.5. Gerüst erstellt & Funktionalität implementiert
 * mak/v2/29.5. Klasse kommentiert
 */

package ch.ffhs.amhs.notesatmobile.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;

import ch.ffhs.amhs.notesatmobile.NotizBearbeitungActivity;

/**
 * OnClickListener für Aktionen auf den Bearbeiten-Button
 */
class ListEntryOnModifyClickListener extends BaseListEntryOnClickListener {

    /**
     * Konstruktor
     * @param parent Parent-Instanz
     * @param entryValue EntryValue Objekt mit Informationen über das Element
     */
    ListEntryOnModifyClickListener(@NonNull final ViewGroup parent,
                                          @NonNull final ListViewEntryValue entryValue) {
        super(parent, entryValue);
    }

    /**
     * OnClick Funktionalität bei Klicken des Bearbeiten-Buttons
     * Hier wird eine neue Bearbeitung-Activity gestartet, welche auf der gewählten Notiz steht.
     * Die Laufende Activity wurd nur angehalten und kann anschliessend wieder fortgesetzt werden.
     * @param v View
     */
    @Override
    public void onClick(View v) {
        // Bundle erstellen
        final Bundle bundle = new Bundle();
        AnzeigeHelper.addNotizToBundle(getEntryValue().getNotizId(), bundle);

        // Intent erstellen und Activity staretn
        final Intent intent = new Intent(getParent().getContext(), NotizBearbeitungActivity.class);
        intent.putExtras(bundle);
        getParent().getContext().startActivity(intent);
    }
}
