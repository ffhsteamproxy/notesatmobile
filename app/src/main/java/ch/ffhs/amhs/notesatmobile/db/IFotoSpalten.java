/*
 * Zweck:
 * Konstantenklasse für die Tabelle Foto
 *
 * Author/Version/Datum:
 * mak/v1/13.5. Grundgerüst
 * hos/v2/13.5. Namensgebung/Name hinzugefügt
 * mak/v3/13.5. Name korrigiert
 */
package ch.ffhs.amhs.notesatmobile.db;

/**
 * Interface Klasse für die Spalten der Tabelle Fotos
 */
public interface IFotoSpalten {

    /**
     * Datenfelder
     */
    String FOTO_ID = "_id";
    String NOTIZ_ID = "NotizId";
    String NAME = "Name";
    String PFAD = "Pfad";
}
