/*
 * Zweck:
 * Listener für Klick-Aktionen auf den Share-Button eines Entries
 * <p>
 * Author/Version/Datum:
 * mak/v1/28.5. Gerüst erstellt
 * hos/v2/4.6. Service einbinden
 * mak/v3/5.6. Delegate für Versenden hinzugefügt
 */

package ch.ffhs.amhs.notesatmobile.ui;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import ch.ffhs.amhs.notesatmobile.NotizenUebersichtActivity;
import ch.ffhs.amhs.notesatmobile.R;
import ch.ffhs.amhs.notesatmobile.data.Notiz;
import ch.ffhs.amhs.notesatmobile.data.NotizHandler;
import ch.ffhs.amhs.notesatmobile.data.PersistenzHelper;
import ch.ffhs.amhs.notesatmobile.lib.BerechtigungenCheck;
import ch.ffhs.amhs.notesatmobile.lib.ISMSSendDelegate;
import ch.ffhs.amhs.notesatmobile.lib.NotesConstants;
import ch.ffhs.amhs.notesatmobile.services.INotizSendenRunnable;
import ch.ffhs.amhs.notesatmobile.services.NotizSendenService;
import ch.ffhs.amhs.notesatmobile.services.NotizSendenServiceConnection;

/**
 * OnClickListener für Aktionen auf den Share-Button
 */
class ListEntryOnShareClickListener extends BaseListEntryOnClickListener {

    /**
     * Service Informationen
     */
    private final Context context = getParent().getContext();
    private final NotizenUebersichtActivity.ContactSearchActivityDelegate activityDelegate;

    /**
     * Konstruktor
     *
     * @param parent     Parent-Instanz
     * @param entryValue EntryValue Objekt mit Informationen über das Element
     */
    ListEntryOnShareClickListener(@NonNull final ViewGroup parent,
                                  @NonNull final ListViewEntryValue entryValue,
                                  @NonNull final NotizenUebersichtActivity.ContactSearchActivityDelegate activityDelegate) {
        super(parent, entryValue);
        this.activityDelegate = activityDelegate;
    }

    /**
     * Runnable Klasse für das Melden, dass die SMS versendet wurde und danach Beenden des Services
     */
    private class NotizListRunnable extends INotizSendenRunnable {

        public void run() {

            // Nachdem das Resultat bekannt ist, wird der Service beendet
            Log.d(NotesConstants.DEBUG_LOG_TAG, ergebnis);

            // Erste Antwort, unbind des Service
            if (ergebnis == NotizSendenService.SMS_SENDING) {
                // Nur für Debugging, um den Ablauf zu sehen
            } else { // weitere Antworten auf dem Display ausgeben

                if (ergebnis == NotizSendenService.SMS_SENT) {
                    Toast.makeText(context, getParent().getContext().getString(R.string.MeldungSMSVersendet, mobilNummer), Toast.LENGTH_LONG).show();


                } else {
                    Toast.makeText(context, R.string.ErrorSMSNichtVersendet, Toast.LENGTH_LONG).show();
                }
                connection.notizSendenServiceBinder.stopStatusUpdate();
                context.unbindService(connection); // der richtige Service befindet sich im Runnable
            }
        }
    }

    /**
     * Beim Klick auf den Button wird die folgende Methode aufgerufen
     *
     * @param v View von der der Aufruf stammt
     */
    @Override
    public void onClick(View v) {

        //BerechtigungenCheck.checkBerechtigungSMS(); ... Todo, die Activity fehlt
        if (BerechtigungenCheck.isBerechtigtFuerSMS(context)) {

            // SMS Daten zusammensuchen
            final Notiz proxy = NotizHandler.erstelleNotizProxy(getEntryValue().getNotizId());
            final Notiz notiz = PersistenzHelper.getInstance(context).ladeNotiz(proxy);

            // Delegates starten
            final SMSSendDelegate smsSendDelegate = new SMSSendDelegate(notiz);
            activityDelegate.setSmsSendDelegate(smsSendDelegate);
            activityDelegate.run();
        }
    }

    /**
     * Innere Delegate-Klasse, welche das Resultat vom Intent versendet
     */
    private class SMSSendDelegate implements ISMSSendDelegate {

        private final Notiz notiz;

        /**
         * Konstruktor
         * @param notiz Notiz-Bean
         */
        public SMSSendDelegate(@NonNull final Notiz notiz) {
           this.notiz = notiz;
        }

        /**
         * Methode, um die SMS abzusenden
         * @param phoneNumber Zielnummer
         */
        @Override
        public void sendSMSToNumber(@NonNull String phoneNumber) {
            // Service starten
            final ServiceConnection notizSendenService = new NotizSendenServiceConnection(new NotizListRunnable(), notiz, phoneNumber); // fire and forget
            Log.d(NotesConstants.DEBUG_LOG_TAG, context.getString(R.string.MeldungStartNotizService));
            final Intent notizSendenIntent = new Intent(context, NotizSendenService.class);
            context.bindService(notizSendenIntent, notizSendenService, Context.BIND_AUTO_CREATE);
        }
    }
}

