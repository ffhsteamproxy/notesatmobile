/*
 * Zweck:
 * Service, um die Notiz zu versenden
 * <p>
 * Author/Version/Datum:
 * mak/v1/13.5. Grundgerüst
 * hos/v2/4.6. Service Implementierung
 */

package ch.ffhs.amhs.notesatmobile.services;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.telephony.SmsManager;
import android.util.Log;

import ch.ffhs.amhs.notesatmobile.R;
import ch.ffhs.amhs.notesatmobile.data.Notiz;
import ch.ffhs.amhs.notesatmobile.lib.NotesConstants;
import ch.ffhs.amhs.notesatmobile.lib.SMSParser;

/**
 * Service zum Senden einer Notiz
 */
public class NotizSendenService extends Service {

    /**
     * Service Variablen
     */
    private final IBinder binder = new NotizSendenServiceBinder();
    private Handler callbackHandler;
    private INotizSendenRunnable runnable;
    private BroadcastReceiver statusReceiver;

    /**
     * Konstanten
     */
    public static final String SMS_SENT = "SMS_SENT";
    public static final String SMS_SEND_ERROR = "SMS_SEND_ERROR";
    public static final String SMS_SENDING ="SMS_SENDING";

    /**
     * Binder für den Service zuweisen
     * @param intent Intent mit den Daten
     * @return Binder für den Service
     */
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        return binder;
    }

    /**
     * Service Binder Klasse mit der Funktionalität
     */
    public class NotizSendenServiceBinder extends Binder {

        /**
         * Callback für die Resultat-Rückgabe setezn
         *
         * @param rHandler übergebener Callback Handler
         */
        public void setzeCallbackHandler(Handler rHandler) {
            callbackHandler = rHandler;
        }

        /**
         * Runnable für das Abfüllen des Resultats setzen
         *
         * @param rRunnable übergebenes Runnable für den Callback
         */
        public void setzeRunnable(final INotizSendenRunnable rRunnable) {
            runnable = rRunnable;
        }

        /**
         * Methode zum Versenden der SMS
         */
        public void sendeSMS(Notiz notiz, String mobilNummer) {

            // Informationen setzen
            runnable.mobilNummer = mobilNummer;

            // SMS Manager und Text
            final SmsManager smsManager = SmsManager.getDefault();
            final String smsText = SMSParser.erstelleSMS(notiz);

            // Intent für Rückmeldung
            final PendingIntent statusIntent = PendingIntent.getBroadcast(getBaseContext(), 0,
                    new Intent(SMS_SENT), 0);

            // SMS versenden
            Log.d(NotesConstants.DEBUG_LOG_TAG, getBaseContext().getString(R.string.MeldungVersendeSMS));
            smsManager.sendTextMessage(mobilNummer, null, smsText, statusIntent, null);

            // Rückmeldung, ob die Nachricht versendet wurde
            statusReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context arg0, Intent arg1) {
                    if (getResultCode() == Activity.RESULT_OK) {
                        runnable.ergebnis = SMS_SENT;
                    } else {
                        runnable.ergebnis = SMS_SEND_ERROR;
                    }
                    callbackHandler.post(runnable);
                }
            };
            registerReceiver(statusReceiver, new IntentFilter(SMS_SENT));

            // Resultat zurückgeben
            runnable.ergebnis = SMS_SENDING;
            callbackHandler.post(runnable);
        }

        public void stopStatusUpdate(){
           unregisterReceiver(statusReceiver);
        }
    }
}