/*
 * Zweck:
 * CursorAdapter für Anzeige der Notizübersicht
 *
 * Author/Version/Datum:
 * mak/v1/26.5. Adapter erstellt
 * mak/v2/27.5. Alert für Löschen / Aktion für Bearbeiten hinzugefügt
 * mak/v3/28.5. Listener ausgelagert, EntryValue für jeden Record eingefügt
 * mak/v4/29.5. Klasse kommentiert
 * hos/v5/31.5. Verschieben der Konstante
 * mak/v6/03.6. Foto/Audio hinzugefügt
 * mak/v7/05.6. Send-Delegate hinzugefügt
 */

package ch.ffhs.amhs.notesatmobile.ui;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import java.util.HashMap;

import ch.ffhs.amhs.notesatmobile.NotizenUebersichtActivity;
import ch.ffhs.amhs.notesatmobile.R;
import ch.ffhs.amhs.notesatmobile.db.INotizSpalten;
import ch.ffhs.amhs.notesatmobile.db.NotizTabelle;
import ch.ffhs.amhs.notesatmobile.db.NotizView;

/**
 * Implementation des CursorAdapters für die Anzeige aller Notizen in der Übersichts-Activity
 */
public class ListViewCursorAdapter extends SimpleCursorAdapter {

    private final LayoutInflater inflater;
    private final HashMap<Integer, ListViewEntryValue> entryList = new HashMap<>();
    private final Runnable delegate;
    private final NotizenUebersichtActivity.ContactSearchActivityDelegate contactDelegate;

    /**
     * Konstruktor
     * @param context Kontext des Adapters (e.g. Activity)
     * @param layout Layout, welches die Einträge haben sollen
     * @param c Cursor der Records
     * @param from Quell-Felder
     * @param to Ziel der Quellfelder
     * @param flags Flags, welche mitgegeben werden können
     */
    public ListViewCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to,
                                 int flags, @NonNull final Runnable delegate,
                                 @NonNull final NotizenUebersichtActivity.ContactSearchActivityDelegate contactDelegate) {
        super(context, layout, c, from, to, flags);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.delegate = delegate;
        this.contactDelegate = contactDelegate;
    }

    /**
     * Bereitet eine Modifizierte View auf, welches zusätzlich Meta-Informationen in jedem Element
     * speichert. Diese Metainformationen werden für das Swipen oder für Aktionen verwendet
     * @param position Position des Records
     * @param convertView View, welche Konvertiert wird.
     * @param parent Parent View.
     * @return Gibt eine Aufbereitete View für die Anzeige zurück
     */
    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {

            // Daten setzen oder ggf. lookupen, falls diese noch nicht vorhanden sind
            if (!entryList.containsKey(position)) {
                // In title Liste hinzufügen
                if (getCursor().moveToPosition(position)) {
                    final long notizId = getCursor().getLong(getCursor().getColumnIndex(INotizSpalten.NOTIZ_ID));
                    final String title = getCursor().getString(getCursor().getColumnIndex(INotizSpalten.TITEL));
                    final String datum = getCursor().getString(getCursor().getColumnIndex(NotizTabelle.SPEICHERZEITPUNKT));
                    final int countFoto = getCursor().getInt(getCursor().getColumnIndex(NotizView.COUNT_FOTO));
                    final int countAudio = getCursor().getInt(getCursor().getColumnIndex(NotizView.COUNT_AUDIO));
                    boolean hasFoto = countFoto > 0;
                    boolean hasAudio = countAudio > 0;
                    entryList.put(position, new ListViewEntryValue(notizId, title, datum, hasFoto, hasAudio));
                }
            }

            convertView = inflater.inflate(R.layout.notizen_uebersicht_list_entry, null);
            final ViewSwitcher viewSwitcher = (ViewSwitcher) convertView.findViewById(R.id.viewswitcher);
            final TextView textView = (TextView) convertView.findViewById(R.id.entry_notiz_titel);
            final TextView datumView = (TextView) convertView.findViewById(R.id.entry_notiz_datum);
            final ImageView fotoImage = (ImageView) convertView.findViewById(R.id.fotoVorhanden);
            final ImageView audioImage = (ImageView) convertView.findViewById(R.id.audioVorhanden);

            final ImageButton deleteButton = (ImageButton) convertView.findViewById(R.id.entry_loesche_notiz);
            deleteButton.setTag(position);
            final ImageButton editButton = (ImageButton) convertView.findViewById(R.id.entry_bearbeite_notiz);
            editButton.setTag(position);
            final ImageButton shareButton = (ImageButton) convertView.findViewById(R.id.entry_teile_notiz);
            shareButton.setTag(position);
            // Viewholder erstellen
            viewHolder = new ViewHolder(viewSwitcher, textView, datumView, fotoImage, audioImage, deleteButton, editButton, shareButton);

            // Listener registrieren
            final ListViewEntryValue entryValue = entryList.get(position);
            viewHolder.getDeleteButton().setOnClickListener(new ListEntryOnDeleteClickListener(parent, entryValue, delegate));
            viewHolder.getEditButton().setOnClickListener(new ListEntryOnModifyClickListener(parent, entryValue));
            viewHolder.getShareButton().setOnClickListener(new ListEntryOnShareClickListener(parent, entryValue, contactDelegate));

            // Tag in die konvertierte View setzen
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (entryList.containsKey(position)) {
            // Korrekten Titel für die View setzen
            viewHolder.getTextView().setText(entryList.get(position).getTitle());
            viewHolder.getDatumView().setText(entryList.get(position).getDatum());
            // Buttons aktivieren
            int fotoShowFlag = entryList.get(position).hasFoto() ? View.VISIBLE : View.INVISIBLE;
            int audioShowFlag = entryList.get(position).hasAudio() ? View.VISIBLE : View.INVISIBLE;
            viewHolder.getFotoImage().setVisibility(fotoShowFlag);
            viewHolder.getAudioImage().setVisibility(audioShowFlag);
        }
        return convertView;
    }

    /**
     * Methode, um einen neuen Cursor zu setzen und die Daten neu zu laden.
     * @param cursor
     */
    @Override
    public void changeCursor(Cursor cursor) {
        super.changeCursor(cursor);
        entryList.clear();
    }
}
