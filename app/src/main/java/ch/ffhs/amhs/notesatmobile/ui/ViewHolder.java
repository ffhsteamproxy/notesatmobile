/*
 * Zweck:
 * Halteinstanz für View-Anzeige pro Notiz-Entry
 *
 * Author/Version/Datum:
 * mak/v1/26.5. Bean erstellt
 * mak/v2/29.5. Klasse kommentiert
 * mak/v3/03.6. Foto/Audio hinzugefügt
 */

package ch.ffhs.amhs.notesatmobile.ui;

import android.support.annotation.NonNull;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewSwitcher;

/**
 * Halte-Instanz für einen Record einer ListView
 */
final class ViewHolder {

    private final ViewSwitcher viewSwitcher;
    private final TextView textView;
    private final TextView datumView;
    private final ImageView fotoImage;
    private final ImageView audioImage;
    private final ImageButton deleteButton;
    private final ImageButton editButton;
    private final ImageButton shareButton;

    /**
     * Konstuktor
     * @param viewSwitcher ViewSwitcher des Records
     * @param textView TextView des Records (NotizId)
     * @param datumView Textview des Records für Datum
     * @param fotoImage ImageView mit FotoIcon
     * @param audioImage ImageView mit AudioIcon
     * @param deleteButton Instanz des Lösch-Buttons
     * @param editButton Instanz des Bearbeiten-Buttons
     * @param shareButton Instanz des Teilen-Buttons
     */
    ViewHolder(@NonNull final ViewSwitcher viewSwitcher, @NonNull final TextView textView, @NonNull final TextView datumView,
               @NonNull final ImageView fotoImage, @NonNull final ImageView audioImage,
               @NonNull final ImageButton deleteButton, @NonNull final ImageButton editButton,
               @NonNull final ImageButton shareButton) {
        this.viewSwitcher = viewSwitcher;
        this.textView = textView;
        this.datumView = datumView;
        this.fotoImage = fotoImage;
        this.audioImage = audioImage;
        this.deleteButton = deleteButton;
        this.editButton = editButton;
        this.shareButton = shareButton;
    }

    /**
     * Gibt den ViewSwitcher der Klasse zurück
     * @return
     */
    @NonNull
    ViewSwitcher getViewSwitcher() {
        return viewSwitcher;
    }

    /**
     * Gibt die TextView des Entrys zurück
     * @return TextView mit Titel der Notiz
     */
    @NonNull
    TextView getTextView() {
        return textView;
    }

    /**
     * Gibt die TextView für das Datum zurück
     * @return TextView mit Datum der Notiz
     */
    @NonNull
    TextView getDatumView() {
        return datumView;
    }

    /**
     * Gibt die ImageView des Foto-Icons zurück
     * @return ImageView des Foto-Icons zurück
     */
    @NonNull
    ImageView getFotoImage() {
        return fotoImage;
    }

    /**
     * Gibt die ImageView des Audio-Icons zurück
     * @return ImageView des Audio-Icons zurück
     */
    @NonNull
    ImageView getAudioImage() {
        return audioImage;
    }

    /**
     * Gibt die Instanz des Lösch-Buttons zurück.
     * @return Instanz des Lösch-Buttons
     */
    @NonNull
    ImageButton getDeleteButton() {
        return deleteButton;
    }

    /**
     * Gibt die Instanz des Bearbeiten-Buttons zurück
     * @return Instanz des Bearbeiten-Buttons
     */
    @NonNull
    ImageButton getEditButton() {
        return editButton;
    }

    /**
     * Gibt die Instanz des Teilen-Buttons zurück
     * @return Instanz des Teilen-Buttons
     */
    @NonNull
    ImageButton getShareButton() {
        return shareButton;
    }
}
