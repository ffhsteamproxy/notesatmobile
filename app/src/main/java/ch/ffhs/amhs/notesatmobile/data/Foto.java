/*
 * Zweck:
 * Transportobjekt für ein Foto
 *
 * Author/Version/Datum:
 * mak/v1/2.5.  Grundgerüst
 * hos/v2/4.5.  Namensgebung
 * mak/v3/29.5. Klasse kommentiert
 */

package ch.ffhs.amhs.notesatmobile.data;

import android.support.annotation.NonNull;

import ch.ffhs.amhs.notesatmobile.lib.IFotoanhang;

/**
 * Transportobjekt für eine Fotoaufnahme
 */
public final class Foto implements IFotoanhang {

    private long fotoId;
    private final String pfad;
    private final String name;

    /**
     * Konstruktor des Foto-Objektes
     * @param fotoId Die Audio-Id aus der Datenbank
     * @param pfad Der physische Pfad der Foto-Datei
     * @param name Der Name der Foto-Datei
     */
    public Foto(long fotoId, @NonNull final String pfad, @NonNull final String name) {
        this.fotoId = fotoId;
        this.pfad = pfad;
        this.name = name;
    }

    /**
     * Gibt die Id der Fotodatei zurück
     * @return Audio-Id als Integer
     */
    public long getFotoId() {
        return fotoId;
    }

    public void setFotoId(final long fotoId) {
        this.fotoId = fotoId;
    }

    /**
     * Gibt den Pfad der Fotodatei zurück
     * @return Pfad als String
     */
    @NonNull
    public String getPfad() {
        return pfad;
    }

    /**
     * Gibt den Namen der Fotodatei zurück
     * @return Name als String
     */
    @Override
    @NonNull
    public String getName() {
        return name;
    }
}
