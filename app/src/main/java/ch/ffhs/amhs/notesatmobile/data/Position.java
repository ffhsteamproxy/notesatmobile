/*
 * Zweck:
 * Transportobjekt für GPS-Koordinaten
 * <p>
 * Author/Version/Datum:
 * mak/v1/2.5. Grundgerüst
 * hos/v3/2.6. double für die Längen und Breitengrade
 */

package ch.ffhs.amhs.notesatmobile.data;

import android.support.annotation.NonNull;

/**
 * Transportobjekt für eine Position
 */
public final class Position {

    /**
     * GPS Informationen
     */
    private double longitude;
    private double latitude;
    private String naehe;

    /**
     * Informationen für die Anzeige
     */
    private final static String KOORDINATEN_DELIMITER = ",";

    /**
     * Erstellen einer neuen Postion
     */
    public Position(@NonNull final double longitude, @NonNull final double latitude, final String naehe) {
        this.longitude = longitude;
        this.latitude = latitude;
        this.naehe = naehe;
    }

    /**
     * Gibt die Longitude der GPS-Position zurück
     *
     * @return
     */
    @NonNull
    public double getLongitude() {
        return longitude;
    }

    /**
     * Gibt die Latitude der GPS-Position zurück
     *
     * @return
     */
    @NonNull
    public double getLatitude() {
        return latitude;
    }

    /**
     * Ort in der Nähe der Position, wenn vorhanden und gesetzt
     *
     * @return Ortsname
     */
    public String getNaehe() {

        return naehe;
    }

    /**
     * Rückgabe eines vernünftigen Anzeige der Ortsdaten
     * @return Name des Orts oder die Koordinaten
     */
    public String getNaeheOderKoordinaten() {
        if (getLongitude() == 0 || getLatitude() == 0)
            return null;
        if (naehe == null || naehe.length() == 0)
            return Double.toString(getLatitude()) + KOORDINATEN_DELIMITER + Double.toString(getLongitude());
        return naehe;
    }
}
