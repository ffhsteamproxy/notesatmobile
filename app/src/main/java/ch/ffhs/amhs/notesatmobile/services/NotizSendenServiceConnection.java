/*
 * Zweck:
 * Auslagern der Service Connection wegen mehrfacher Verwendung
 * <p>
 * Author/Version/Datum:
 * hos/v1/4.6. Service Implementierung
 */

package ch.ffhs.amhs.notesatmobile.services;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;

import ch.ffhs.amhs.notesatmobile.data.Notiz;

/**
 * Notiz Senden Service Connection
 */
public class NotizSendenServiceConnection implements ServiceConnection {

    /**
     * Service Informationen
     */
    private Handler notizCallbackHandler;
    public NotizSendenService.NotizSendenServiceBinder notizSendenServiceBinder;
    private INotizSendenRunnable callbackRunnable;

    /**
     * Informationen für die Ausführung
     */
    private Notiz notiz;
    private String mobilNummer;

    /**
     * Der Standard Konstruktor sollte nicht verwendet werden
     */
    private NotizSendenServiceConnection() {
    }

    /**
     * Konstruktor mit den benötigten Attributen
     *
     * @param rRunnable Runnable für den Callback
     * @param rNotiz zu Versendende Notiz
     * @param rMobilNummer Nummer, an die versendet werden soll
     */
    public NotizSendenServiceConnection(INotizSendenRunnable rRunnable, Notiz rNotiz, String rMobilNummer) {

        callbackRunnable = rRunnable;
        notiz = rNotiz;
        mobilNummer = rMobilNummer;
    }

    /**
     * Methodenaufruf beim Verbinden des Dienstes
     *
     * @param componentName
     * @param iBinder
     */
    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {

        // Service Informationen setzen
        callbackRunnable.connection = this;

        // Aktionen ausführen
        notizCallbackHandler = new Handler();
        notizSendenServiceBinder = (NotizSendenService.NotizSendenServiceBinder) iBinder;
        notizSendenServiceBinder.setzeCallbackHandler(notizCallbackHandler);
        notizSendenServiceBinder.setzeRunnable(callbackRunnable);
        notizSendenServiceBinder.sendeSMS(notiz, mobilNummer);
    }

    /**
     * Methodenaufruf beim schliessen der Verbindung
     *
     * @param componentName
     */
    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        notizSendenServiceBinder = null;
        notizCallbackHandler = null;
    }
}
