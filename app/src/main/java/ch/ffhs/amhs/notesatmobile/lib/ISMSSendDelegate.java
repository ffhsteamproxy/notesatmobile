/*
 * Zweck:
 * Delegate-Interface
 *
 * mak/v1/5.6. Interface erstellt
 */
package ch.ffhs.amhs.notesatmobile.lib;

import android.support.annotation.NonNull;

/**
 * Delegate für Callback, um eine SMS zu versenden
 */
public interface ISMSSendDelegate {

    /**
     * Methode, um das Senden einer SMS auszulösen
     * @param phoneNumber Zielnummer
     */
    void sendSMSToNumber(@NonNull final String phoneNumber);
}
