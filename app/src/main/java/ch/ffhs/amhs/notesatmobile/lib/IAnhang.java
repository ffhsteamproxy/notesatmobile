/*
 * Zweck:
 * Interface für den Anhang einer Notiz
 *
 * Author/Version/Datum:
 * mak/v1/13.5. Grundgerüst
 * mak/v2/29.5. Klasse kommentiert
 */
package ch.ffhs.amhs.notesatmobile.lib;

/**
 * Interfaceklasse für alle Anhänge
 */
public interface IAnhang {

    /**
     * Gibt den Namen des Anhangs zurück
     * @return Name als String
     */
    String getName();
}
