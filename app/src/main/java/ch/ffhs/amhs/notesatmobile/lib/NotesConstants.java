/*
 * Zweck:
 * Konstanten-Klasse für allgemeine Konstanten
 *
 * Author/Version/Datum:
 * mak/v1/02.5. Grundgerüst
 * mak/v2/15.5. Klammer, Semikolon und Komma hinzugefügt
 * mak/v3/26.5. Space hinzugefügt
 * hos/v4/31.5. Verschieben der Konstante, Logging Tags
 *
 */
package ch.ffhs.amhs.notesatmobile.lib;

public final class NotesConstants {

    /**
     * Allgemeine Konstanten
     */
    // TODO, bitte keine weissen Schimmel hier, nicht mehr als der eine der schon da ist und mit new String() ersetzt werden könnte
    public static final String EMPTY_STRING = "";

    /** Logging Tags */
    public static final String ERROR_LOG_TAG = "NOTES_ERROR";
    public static final String DEBUG_LOG_TAG = "NOTES_DEBUG";

}
