/*
 * Zweck:
 * Übersicht über die Notizen Startscreen
 *
 * Author/Version/Datum:
 * hos/v1/8.4. Grundgerüst
 * hos/v2/4.5. Namensgebung
 * mak/v3/22.5 SimpleCursorAdapter hinzugefügt
 * mak/v4/25.5 SimpleCursorAdapter mit Liste verknüpft / onPostResume hinzugefügt
 * mak/v5/26.5 Gesten hinzugefügt
 * mak/v6/29.5. Klasse kommentiert
 * mak/v7/1.6. Suche implementiert
 * hos/v8/4.5. Kommentare erweitert
 */

package ch.ffhs.amhs.notesatmobile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ListView;

import ch.ffhs.amhs.notesatmobile.data.PersistenzHelper;
import ch.ffhs.amhs.notesatmobile.db.NotizTabelle;
import ch.ffhs.amhs.notesatmobile.lib.BerechtigungenCheck;
import ch.ffhs.amhs.notesatmobile.lib.ISMSSendDelegate;
import ch.ffhs.amhs.notesatmobile.lib.NotesConstants;
import ch.ffhs.amhs.notesatmobile.ui.AnzeigeHelper;
import ch.ffhs.amhs.notesatmobile.ui.ListViewCursorAdapter;
import ch.ffhs.amhs.notesatmobile.ui.NotizenGestureListener;

/**
 * Aktivität der Übersicht
 */
public class NotizenUebersichtActivity extends AppCompatActivity {

    public static final int CONTACT = 0;
    private ISMSSendDelegate smsSendDelegate;

    /**
     * OnCreate Methode, welche beim Aufbau der Activity aufgerufen wird.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notizen_uebersicht);
        Toolbar toolbar = (Toolbar) findViewById(R.id.mainToolbar);
        setTitle(NotesConstants.EMPTY_STRING);
        setSupportActionBar(toolbar);
        initListView();
        initSearchField();

        // Check der Berechtigungen der Applikation
        BerechtigungenCheck.checkBerechtigung(this);
    }

    /**
     * OnPostResume, welche bei Fortsetzen der Activity (z.B. nach Rückkehr aus einem Intent)
     * aufgerufen wird. Hierbei wird die View für die Übersicht neu erstellt, damit die Änderungen
     * ersichtlich werden.
     */
    @Override
    protected void onPostResume() {
        super.onPostResume();
        ListView lv = (ListView) findViewById(R.id.mainListView);
        // Gemäss Android-SDK soll für den Reload einer View jeweils ein neuer Cursor verwendet
        // werden. Da der Reload der neuen Daten so nicht funktioniert, wird der bestehende Adapter
        // mit einem neuen Adapter und neuem Cursor ersetzt.
        final Cursor cursor = erstelleViewCursor();
        initAdapter(lv, cursor);
    }

    /**
     * Resultat-Methode
     * @param requestCode RequestCode
     * @param resultCode Resultat-Code
     * @param data Daten des Intents
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CONTACT) {
            if (resultCode == Activity.RESULT_OK) {
                Uri contactData = data.getData();
                final Cursor c = getContentResolver().query(contactData, null, null, null, null);
                if (c.moveToFirst()) {
                    final String number = c.getString(c.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    if (smsSendDelegate != null) {
                        smsSendDelegate.sendSMSToNumber(number);
                        smsSendDelegate = null;
                    }
                }
                c.close();
            }
        }
    }

    /**
     * Delegate setzen und starten
     * @param delegate Delegate-Klasse
     */
    public void setDelegateAndStartContactIntent(@NonNull final ISMSSendDelegate delegate) {
        this.smsSendDelegate = delegate;
        final Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
        startActivityForResult(intent, CONTACT);
    }

    /**
     * Initialisiert die Liste mit allen Notizen
     */
    private void initListView() {
        ListView lv = (ListView) findViewById(R.id.mainListView);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Intent erstellen
                startNotizAnzeige(id);
            }
        });

        // Gesten einfügen
        final GestureDetector gestureDetector = new GestureDetector(this, new NotizenGestureListener(lv));
        lv.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        });

        final Cursor cursor = erstelleViewCursor();
        initAdapter(lv, cursor);
    }

    /**
     * Hilfsmethode zum Öffnen einer persistent gespeicherten Notiz
     * @param notizId id der Notiz
     */
    private void startNotizAnzeige(long notizId) {

        // Bundle erstellen
        final Bundle bundle = new Bundle();

        AnzeigeHelper.addNotizToBundle(notizId, bundle);

        // Intent erstellen und Activity staretn
        final Intent intent = new Intent(this, NotizAnzeigeActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    /**
     * Activity Bearbeiten für eine neue Notiz öffnen
     * @param view
     */
    public void onNotizErstellen(final View view){
        startActivity(new Intent(this, NotizBearbeitungActivity.class));
    }

    /**
     * Erstellt einen Cursor für die View
     * @return Cursor, welcher Positioniert auf alle Notizen ist.
     */
    @NonNull
    private Cursor erstelleViewCursor() {
        return PersistenzHelper.getInstance(this).sucheNotiz(NotesConstants.EMPTY_STRING);
    }

    /**
     * Initialisiert den CursorAdapter für die Liste
     * @param listView ListView Objekt
     * @param cursor Aktueller Cursor
     */
    private void initAdapter(@NonNull final ListView listView, @NonNull final Cursor cursor) {
        final ListViewCursorAdapter sca = new ListViewCursorAdapter(this, R.layout.notizen_uebersicht_list_entry, cursor, new String[]{NotizTabelle.TITEL},
                new int[]{R.id.entry_notiz_titel, R.id.entry_notiz_datum}, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER, new DeleteDelegate(), new ContactSearchActivityDelegate());
        listView.setAdapter(sca);
    }

    /**
     * Initialisiert das Suchfeld und setzt den Text auf Leer
     */
    private void initSearchField() {
        final EditText editText = (EditText) findViewById(R.id.suchFeld);
        editText.addTextChangedListener(new SearchAction(this));
        editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Text entfernen
                editText.setText(NotesConstants.EMPTY_STRING);
            }
        });
    }

    /**
     * Private Klasse, um den After-Run Anstoss nach dem Löschen zu delegieren.
     */
    private final class DeleteDelegate implements Runnable {

        @Override
        public void run() {
          onPostResume();
        }
    }

    /**
     * Innere Klasse für die Suche
     */
    private final class SearchAction implements TextWatcher {

        /**
         * Applikationskontext
         */
        private final Context context;

        /**
         * Suche Erstellen
         * @param context Applikationskontext
         */
        private SearchAction(@NonNull final Context context) {
           this.context = context;
        }

        /**
         * Methode des Interfacae für vor der Textänderung
         * @param s
         * @param start
         * @param count
         * @param after
         */
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // do nothing
        }

        /**
         * Methode des Interface für während der Textänderung
         * @param s
         * @param start
         * @param before
         * @param count
         */
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            // do nothing
        }

        /**
         * Methode des Interface für nach der Textänderung und Initialisierung der Suche
         * @param s
         */
        @Override
        public void afterTextChanged(Editable s) {
            // View updaten
            final Cursor searchCursor = PersistenzHelper.getInstance(context).sucheNotiz(s.toString());
            final ListView lv = (ListView) findViewById(R.id.mainListView);
            initAdapter(lv, searchCursor);
        }
    }

    /**
     * Innerklasse für Delegates
     */
    public class ContactSearchActivityDelegate implements Runnable {

        private ISMSSendDelegate smsSendDelegate;

        /**
         * Startmethode, um das Kontaktbuch zu starten.
         */
        @Override
        public void run() {
            setDelegateAndStartContactIntent(smsSendDelegate);
        }

        /**
         * Setter um den Delegate zu setzen.
         * @param smsSendDelegate
         */
        public void setSmsSendDelegate(@NonNull final ISMSSendDelegate smsSendDelegate) {
            this.smsSendDelegate = smsSendDelegate;
        }
    }
}