/*
 * Zweck:
 * Konstantenklasse für die Tabelle Stichwort
 *
 * mak/v1/13.5. Grundgerüst
 * hos/v2/13.5. Namensgebung
 */
package ch.ffhs.amhs.notesatmobile.db;

/**
 * Interface Klasse für die Spalten der Tabelle Stichwort
 */
public interface IStichwortSpalten {

    /**
     * Datenfelder
     */
    String NOTIZ_ID = "NotizId";
    String STICHWORT_TEXT = "StichwortText";
}
