/*
 * Zweck:
 * Tabellenabbildung für eine Audio Aufnahme
 *
 * Author/Version/Datum:
 * hos/v1/17.5. Grundgerüst
 * mak/v2/15.5. Audio / Foto Tabellen Create Stmt.
 * mak/v3/25.5. Autoincrement hinzugefügt
 * mak/v4/29.5. Klasse kommentiert
 * hos/v5/30.5. FK, PK Filter
 * mak/v6/03.6. Select Statement entfernt
 */

package ch.ffhs.amhs.notesatmobile.db;

/**
 * Konstantenklasse für Funktionalitäten der Audio-Tabelle
 */
public final class AudioTabelle implements IAudioSpalten {

    // Tabellenname
    public static final String TABELLEN_NAME = "Audio";

    // Scripts - Tabellen erstellen
    public static final String SQL_CREATE = "" +
            "CREATE TABLE " + TABELLEN_NAME + " (" +
            AUDIO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
            NOTIZ_ID + " INT NOT NULL," +
            NAME + " TEXT," +
            PFAD + " TEXT NOT NULL," +
            "CONSTRAINT FKAudioNotizId FOREIGN KEY (" + NOTIZ_ID +") " +
                "REFERENCES " + NotizTabelle.TABELLEN_NAME + " (" + NOTIZ_ID +"));";

    /**
     * CRUD - Where Clause für Notiz Foreign Key, Massen-Löschen
     */
    public static final String FK_FILTER = "" + // Löschen aller Stichworte
            NOTIZ_ID + " = ?;";
    /**
     * CRUD - Where Clause für Primary Key
     */
    public static final String PK_FILTER = "" + // Löschen eines Stichwort
            AUDIO_ID + " = ? ";
}
