/*
 * Zweck:
 * Transportobjekt für ein Audio
 *
 * Author/Version/Datum:
 * mak/v1/2.5.  Grundgerüst
 * mak/v2/29.5. Klasse kommentiert
 */
package ch.ffhs.amhs.notesatmobile.data;

import android.support.annotation.NonNull;

import ch.ffhs.amhs.notesatmobile.lib.IAudioanhang;

/**
 * Transportobjekt für eine Audioaufnahme
 */
public final class Audio implements IAudioanhang {

    private long audioId;
    private final String pfad;
    private final String name;

    /**
     * Konstruktor des Audio-Objektes
     * @param audioId Die Audio-Id aus der Datenbank
     * @param pfad Der physische Pfad der Audio-Datei
     * @param name Der Name der Audio-Datei
     */
    public Audio(final long audioId, @NonNull final String pfad, @NonNull final String name) {
        this.audioId = audioId;
        this.pfad = pfad;
        this.name = name;
    }

    /**
     * Gibt die Id der Audiodatei zurück
     * @return Audio-Id als Integer
     */
    public long getAudioId() {
        return audioId;
    }

    public void setAudioId(final long audioId) {
        this.audioId = audioId;
    }

    /**
     * Gibt den Pfad der Audiodatei zurück
     * @return Pfad als String
     */
    @NonNull
    public String getPfad() {
        return pfad;
    }

    /**
     * Gibt den Namen der Audiodatei zurück
     * @return Name als String
     */
    @Override
    @NonNull
    public String getName() {
        return name;
    }
}
