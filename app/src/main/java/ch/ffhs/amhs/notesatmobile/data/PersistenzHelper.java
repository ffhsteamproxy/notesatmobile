/*
 * Zweck:
 * Persistenz-Layer Zugriffsklasse
 * <p>
 * Author/Version/Datum:
 * mak/v1/13.5. Grundgerüst
 * mak/v2/20.5. findNotiz hinzugefügt
 * hos/v3/21.5. Vorschlag bezüglich der Abfragen
 * mak/v4/22.5  Delete für Audio/Foto hinzugefügt
 * mak/v5/27.5. Löschen der Notiz mit ID implementiert
 * mak/v6/28.5. Create und Update implementiert
 * hos/v7/28.5. Loeschen, Package Change, Lade Notiz (neue Felder), Save mit Postion
 * mak/v8/29.5. Select von Stichworten hinzugefügt / Methoden kommentiert
 * hos/v9/30.5. Delete Anpassungen, Read Stichworte mit .query()
 * hos/v10/31.5. Leere Location speichern
 * hos/v11/2.6. double für die Längen und Breitengrade
 * mak/v12/3.6. Select für Foto/Audio angepasst
 * hos/v13/4.6. Konsequente Anpassung, dass überall sauber Transaktionen / try finally verwendet werden
 * hos/v14/4.6. Löschmechanismus und Delta der Anhänge bei Updates
 * hos/v15/4.6. Speicherung einzelner Anhänge
 */
package ch.ffhs.amhs.notesatmobile.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;

import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import ch.ffhs.amhs.notesatmobile.db.AudioTabelle;
import ch.ffhs.amhs.notesatmobile.db.FotoTabelle;
import ch.ffhs.amhs.notesatmobile.db.IAudioSpalten;
import ch.ffhs.amhs.notesatmobile.db.IFotoSpalten;
import ch.ffhs.amhs.notesatmobile.db.INotizSpalten;
import ch.ffhs.amhs.notesatmobile.db.IStichwortSpalten;
import ch.ffhs.amhs.notesatmobile.db.NotizDatenbank;
import ch.ffhs.amhs.notesatmobile.db.NotizTabelle;
import ch.ffhs.amhs.notesatmobile.db.NotizView;
import ch.ffhs.amhs.notesatmobile.db.StichwortTabelle;

/**
 * Persistenzklasse für den Zugriff der SQLite Daten und Mapping auf die Objekte
 */
public class PersistenzHelper {

    /**
     * Datenbank Eigenschaften und Hilfsdaten
     */
    private NotizDatenbank db;

    /**
     * Singleton Instanz
     */
    private static PersistenzHelper instance;
    private int d;

    /**
     * Neuen Persistenzhelper erstellen
     *
     * @param context
     */
    private PersistenzHelper(@NonNull final Context context) {

        // Datenbank-Zugriff einrichten
        db = NotizDatenbank.getInstance(context);

    }

    /**
     * Singleton Zugriff
     *
     * @param context Applikationskontext
     * @return Singleton Instanz der Klasse
     */
    public static PersistenzHelper getInstance(@NonNull final Context context) {
        if (instance == null) {
            instance = new PersistenzHelper(context);
        }
        return instance;
    }

    /**
     * Laden einer Notiz mittels mitgegebenen Proxy (nach Notiz-Id)
     *
     * @param proxy Proxy, welches mindestens eine Notiz-Id enthält
     * @return Gibt eine vollständig geladene Notiz zurück.
     */
    public Notiz ladeNotiz(@NonNull final Notiz proxy) {

        // Cursor mit allen Feldern für den Zugriff auf eine Notiz
        final Cursor c = db.getReadableDatabase().query(
                NotizTabelle.TABELLEN_NAME,
                new String[]{INotizSpalten.NOTIZ_ID, INotizSpalten.TITEL, INotizSpalten.DETAILS, INotizSpalten.SPEICHERZEITPUNKT, INotizSpalten.AENDERUNGSZEITPUNKT, INotizSpalten.LONGITUDE,
                        INotizSpalten.LATITUDE, INotizSpalten.NAEHE},
                NotizTabelle.PK_FILTER,
                new String[]{String.valueOf(proxy.getNotizId())},
                null,
                null,
                null);

        // Neue leere Notiz erstellen
        final Notiz result = NotizHandler.erstelleNotiz();

        // Resultat (ersten Eintrag) durchgehen und in Notiz abfüllen
        try {
            if (c.moveToFirst()) {

                // Texte abfüllen
                result.setNotizId(c.getInt(c.getColumnIndex(INotizSpalten.NOTIZ_ID)));
                result.setTitel(c.getString(c.getColumnIndex(INotizSpalten.TITEL)));
                result.setDetails(c.getString(c.getColumnIndex(INotizSpalten.DETAILS)));

                // Zeitpunkte abfüllen
                final String erstelltString = c.getString(c.getColumnIndex(INotizSpalten.SPEICHERZEITPUNKT));
                final Timestamp erstelltZeitpunkt = Timestamp.valueOf(erstelltString);
                result.setSpeicherungszeitpunkt(erstelltZeitpunkt);

                final String bearbeitetString = c.getString(c.getColumnIndex(INotizSpalten.AENDERUNGSZEITPUNKT));
                final Timestamp bearbeitetZeitpunkt = Timestamp.valueOf(bearbeitetString);
                result.setBearbeitetZeitpunkt(bearbeitetZeitpunkt);

                // Position abfüllen
                final Position position = new Position(
                        c.getDouble(c.getColumnIndex(INotizSpalten.LONGITUDE)),
                        c.getDouble(c.getColumnIndex(INotizSpalten.LATITUDE)),
                        c.getString(c.getColumnIndex(INotizSpalten.NAEHE))
                );
                result.setPosition(position);

                // Stichworte hinzufügen
                fuegeStichworteHinzu(result);

                // Fotos hinzufügen
                fuegeFotosHinzu(result);

                // Audio hinzufügen
                fuegeAudioHinzu(result);
            }
        } finally {
            c.close();
        }
        return result;
    }

    /**
     * Speichern / Updaten einer Notiz. Die Notiz wird neu gespeichert, wenn die NotizId = 0 ist,
     * ansonsten wird diese geupdatet. Bei Neuerfassung wird automatisch die neue NotizId in das
     * mitgegebene Objekt geschrieben.
     *
     * @param notiz Ausgefüllte Notiz
     */
    public void speichereNotiz(@NonNull final Notiz notiz) {

        // Notiz aktualisieren
        NotizHandler.preSafeUpdate(notiz);

        // Neue notiz
        try {
            db.getWritableDatabase().beginTransaction();

            // Notizdaten
            final ContentValues notizValues = getDbBasisWerte(notiz);

            // INSERT oder UPDATE?
            if (notiz.getNotizId() == 0) {

                // Zusätzliche Daten speichern, wenn die Aufgabe neu ist
                notizValues.put(INotizSpalten.SPEICHERZEITPUNKT, notiz.getSpeicherungszeitpunkt().toString());

                // GPS Daten, nur beim Einfügen
                if (notiz.getPosition() != null) {
                    notizValues.put(INotizSpalten.LONGITUDE, notiz.getPosition().getLongitude());
                    notizValues.put(INotizSpalten.LATITUDE, notiz.getPosition().getLatitude());
                    notizValues.put(INotizSpalten.NAEHE, notiz.getPosition().getNaehe());
                }

                // Notiz ID auf der Entity setzen
                final long notizId = db.getWritableDatabase().insert(NotizTabelle.TABELLEN_NAME, null, notizValues);
                notiz.setNotizId(notizId);

            } else {
                db.getWritableDatabase().update(NotizTabelle.TABELLEN_NAME, notizValues, NotizTabelle.PK_FILTER, new String[]{String.valueOf(notiz.getNotizId())});

            }
            // Stichworte
            dbSpeichereStichworte(notiz.getStichworte(), notiz.getNotizId());

            // Anhänge werden einzeln an eine gespeicherte Notiz gehängt, sie können nicht unabhängig gelöscht werdsen

            // COMMIT
            db.getWritableDatabase().setTransactionSuccessful();

        } finally {
            db.getWritableDatabase().endTransaction();

        }
    }

    /**
     * Hilfmethode für Basisdaten, die gespeichert werden
     *
     * @param notiz Notiz, deren Daten gepeichert werden sollen
     * @return Datensatz mit den Natizdaten
     */
    private ContentValues getDbBasisWerte(Notiz notiz) {

        // Neues Set von Daten erstellen
        final ContentValues notizValues = new ContentValues();
        notizValues.put(INotizSpalten.TITEL, notiz.getTitel());
        notizValues.put(INotizSpalten.DETAILS, notiz.getDetails());
        notizValues.put(INotizSpalten.AENDERUNGSZEITPUNKT, notiz.getVeraenderungszeitpunkt().toString());
        return notizValues;
    }

    /**
     * Hilfsmethode: Stichtworte einer Aufgabe mit DELETE in die Datenbank speichern ohne Transaktion
     *
     * @param stichworte zu speichernde Stichworte
     * @param notizId    ID der zu speichernden Notiz
     */
    private void dbSpeichereStichworte(List<String> stichworte, Long notizId) {

        // Interner Methodenaufruf, Transaktion im Aufruf

        // Der DROP für den UPDATE spielt beim INSERT keine Rolle -> hier wird alles geflusht
        db.getWritableDatabase().delete(StichwortTabelle.TABELLEN_NAME, StichwortTabelle.FK_FILTER, new String[]{Long.toString(notizId)});

        // Stichworte auf Datenbank speichern
        for (String wort : stichworte) {
            ContentValues stichwortEintrag = new ContentValues();
            stichwortEintrag.put(StichwortTabelle.NOTIZ_ID, Long.toString(notizId));
            stichwortEintrag.put(StichwortTabelle.STICHWORT_TEXT, wort);
            db.getWritableDatabase().insert(StichwortTabelle.TABELLEN_NAME, null, stichwortEintrag);
        }

    }

    /**
     * Ein Foto in einer Notiz speichern
     *
     * @param foto    Zu speicherndes Foto
     * @param notizId NotizId
     */
    public Foto speichereFoto(@NonNull final Foto foto, @NonNull final Long notizId) {

        try {
            db.getWritableDatabase().beginTransaction();

            // Foto auf der Datenbank speichern
            final ContentValues fotoEintrag = new ContentValues();
            fotoEintrag.put(FotoTabelle.NOTIZ_ID, Long.toString(notizId));
            fotoEintrag.put(FotoTabelle.NAME, foto.getName());
            fotoEintrag.put(FotoTabelle.PFAD, foto.getPfad());
            final long fotoId = db.getWritableDatabase().insert(FotoTabelle.TABELLEN_NAME, null, fotoEintrag);
            foto.setFotoId(fotoId);
            db.getWritableDatabase().setTransactionSuccessful();
            return foto;
        } finally {
            db.getWritableDatabase().endTransaction();
        }

        // Das Foto muss hier nicht gespeichert werden, es existiert schon
    }

    /**
     * Einen Audio-Eintrag in einer Notiz speichern
     *
     * @param audio   zu speichernde Audio-Aufnahme
     * @param notizId NotizId
     */

    public Audio speichereAudio(@NonNull final Audio audio, @NonNull final Long notizId) {

        try {
            db.getWritableDatabase().beginTransaction();

            // Audio-Eintrag auf der Datenbank speichern
            final ContentValues audioEintrag = new ContentValues();
            audioEintrag.put(AudioTabelle.NOTIZ_ID, Long.toString(notizId));
            audioEintrag.put(AudioTabelle.NAME, audio.getName());
            audioEintrag.put(AudioTabelle.PFAD, audio.getPfad());
            long audioId = db.getWritableDatabase().insert(AudioTabelle.TABELLEN_NAME, null, audioEintrag);
            audio.setAudioId(audioId);
            db.getWritableDatabase().setTransactionSuccessful();
            return audio;
        } finally {
            db.getWritableDatabase().endTransaction();
        }

        // TODO, Audio speichern?

    }

    /**
     * Löscht eine Notiz mittels mitgegebener Id inkl. allen Referenzen.
     *
     * @param notizId NotizId der Notiz.
     */
    public void loescheNotizFromId(final long notizId) {

        // ID als String für die Filterübergabe
        String[] pkFilterArgs = new String[]{Long.toString(notizId)};

        try {
            db.getWritableDatabase().beginTransaction();
            // Notiz löschen
            db.getWritableDatabase().delete(NotizTabelle.TABELLEN_NAME, NotizTabelle.PK_FILTER, pkFilterArgs);
            // Stichworte loeschen
            db.getWritableDatabase().delete(StichwortTabelle.TABELLEN_NAME, StichwortTabelle.FK_FILTER, pkFilterArgs);
            // Beim Löschen werden auch die Anhänge mitgelöscht
            db.getWritableDatabase().delete(FotoTabelle.TABELLEN_NAME, FotoTabelle.FK_FILTER, pkFilterArgs);
            db.getWritableDatabase().delete(AudioTabelle.TABELLEN_NAME, AudioTabelle.FK_FILTER, pkFilterArgs);

            db.getWritableDatabase().setTransactionSuccessful();
        } finally {
            db.getWritableDatabase().endTransaction();
        }
    }

    /**
     * Löscht eine Notiz mittels mitgegebenem Notiz-Objekt
     *
     * @param notiz Notizobjekt
     */
    public void loescheNotiz(@NonNull final Notiz notiz) {
        loescheNotizFromId(notiz.getNotizId());
    }

    /**
     * Sucht eine Notiz anhand des mitgegebenen Textes.
     * Der Text kann in Titel, Beschreibung, Erfassungsdatum oder Änderungsdatum vorkommen.
     *
     * @param text Suchtext
     * @return Gibt einen Cursor für die Anzeige in der View zurück.
     */
    public Cursor sucheNotiz(@NonNull final String text) {

        // Der Filterstring beinhaltet mehrere Platzhalter, diese müssen alle im Array erfasst werden
        // Hier wird mehrfach auf den selben String gesucht, deshalb ist es ein Array mit n gleichen Einträgen

        // Lokalisierung des Datums wird nciht gemacht (Position 4,5)
        String[] filterArgs = new String[6];
        String filterString = "%" + text + "%";
        for (int i = 0; i < filterArgs.length; i++) {
            filterArgs[i] = filterString;
        }

        // ANF.FU02.1 Suchen einer Notiz
        return db.getReadableDatabase().query(
                NotizView.VIEW_NAME,
                new String[]{NotizView.NOTIZ_ID, NotizView.TITEL, NotizView.SPEICHERZEITPUNKT, NotizView.NAEHE, NotizView.COUNT_FOTO, NotizView.COUNT_AUDIO},
                NotizView.SEARCH_FILTER,
                filterArgs, // ?, ?, ?, sollte wenn möglich optimiert werden
                null,
                null,
                NotizView.SPEICHERZEITPUNKT + " DESC" // absteigend nach Datum sortiert gemäss Anforderung
        );
    }

    /**
     * Löscht Audiodateien anhand der mitgegebenen Liste.
     *
     * @param daten Liste von Audio Dateien
     */
    public void loescheAudio(@NonNull final List<Audio> daten) {

        boolean transactionSuccessful = false;
        try {
            db.getWritableDatabase().beginTransaction();
            //Audiodateieinträge durchgehen und löschen
            for (Audio audio : daten) {
                db.getWritableDatabase().delete(AudioTabelle.TABELLEN_NAME, AudioTabelle.PK_FILTER, new String[]{Long.toString(audio.getAudioId())});
            }
            db.getWritableDatabase().setTransactionSuccessful();
            transactionSuccessful = true;

        } finally {
            db.getWritableDatabase().endTransaction();
        }

        // Die Dateien werden erst gelöscht (en Block), wenn die Transaktion sicher durchgelaufen ist.
        // Nur so ist ein sauberer Rollback möglich
        if (transactionSuccessful) {
            for (Audio audio : daten) {
                loescheDatei(audio.getPfad());
            }
        }
    }

    /**
     * Wrapper Methode für das Löschen einer Audio-Aufnahmen
     * @param audio eine zu löschende Audio-Aufnahme
     */
    public void loescheAudio(Audio audio){
        List<Audio> loeschenListe = new ArrayList<>();
        loeschenListe.add(audio);
        loescheAudio(loeschenListe);
    }

    /**
     * Löscht Fotodateien anhand der mitgegebenen Liste
     *
     * @param daten Liste von Foto Dateien
     */
    public void loescheFoto(@NonNull final List<Foto> daten) {

        boolean transactionSuccessful = false;

        try {
            db.getWritableDatabase().beginTransaction();
            // Fotos durchgehen und löschen
            for (Foto foto : daten) {
                db.getWritableDatabase().delete(FotoTabelle.TABELLEN_NAME, FotoTabelle.PK_FILTER, new String[]{Long.toString(foto.getFotoId())});
            }
            db.getWritableDatabase().setTransactionSuccessful();
            transactionSuccessful = true;
        } finally {
            db.getWritableDatabase().endTransaction();
        }

        // Die Dateien werden erst gelöscht (en Block), wenn die Transaktion sicher durchgelaufen ist.
        // Nur so ist ein sauberer Rollback möglich
        if (transactionSuccessful) {
            for (Foto foto : daten) {
                loescheDatei(foto.getPfad());
            }
        }
    }

    /**
     * Wrapper Methode für das Löschen eines Fotos
     * @param foto ein zu löschendes Foto
     */
    public void loescheFoto(Foto foto){
        List<Foto> loeschenListe = new ArrayList<>();
        loeschenListe.add(foto);
        loescheFoto(loeschenListe);
    }

    /**
     * Fügt Audiodateien in ein Notizobjekt hinzu.
     *
     * @param notiz Notiz-Objekt, welches die Audiodateien erhalten soll. Die Id ist hierbei
     *              die Notiz-Id
     */
    private void fuegeAudioHinzu(@NonNull final Notiz notiz) {
        final Cursor audioCursor = db.getReadableDatabase().query(
                AudioTabelle.TABELLEN_NAME,
                new String[]{AudioTabelle.AUDIO_ID, AudioTabelle.NAME, AudioTabelle.PFAD},
                AudioTabelle.FK_FILTER,
                new String[]{String.valueOf(notiz.getNotizId())},
                null,
                null,
                null);

        try {
            // Audioeintrag (erster Eintrag) hinzufügen
            if (audioCursor.moveToFirst()) {
                do {
                    final Audio audio = new Audio(
                            audioCursor.getInt(audioCursor.getColumnIndex(IAudioSpalten.AUDIO_ID)),
                            audioCursor.getString(audioCursor.getColumnIndex(IAudioSpalten.PFAD)),
                            audioCursor.getString(audioCursor.getColumnIndex(IAudioSpalten.NAME))
                    );
                    notiz.addAudio(audio);
                } while (audioCursor.moveToNext());
            }
        } finally {
            audioCursor.close();
        }
    }

    /**
     * Fügt Stichworte in ein Notizobjekt hinzu.
     *
     * @param notiz Notiz-Objekt, welches die Stichworte erhalten soll. Die Id ist hierbei
     *              die Notiz-Id
     */
    private void fuegeStichworteHinzu(@NonNull final Notiz notiz) {

        final Cursor stichwortCursor = db.getReadableDatabase().query(
                StichwortTabelle.TABELLEN_NAME,
                new String[]{StichwortTabelle.NOTIZ_ID, StichwortTabelle.STICHWORT_TEXT},
                StichwortTabelle.FK_FILTER,
                new String[]{String.valueOf(notiz.getNotizId())},
                null,
                null,
                null);

        // Cursor durchgehen und Werte auslesen
        try {
            if (stichwortCursor.moveToFirst()) {
                do {
                    notiz.addStichwort(stichwortCursor.getString(stichwortCursor.getColumnIndex(IStichwortSpalten.STICHWORT_TEXT)));
                } while (stichwortCursor.moveToNext());
            }
        } finally {
            stichwortCursor.close();
        }
    }

    /**
     * Fügt Fotodateien in ein Notizobjekt hinzu.
     *
     * @param notiz Notiz-Objekt, welches die Fotodateien erhalten soll. Die Id ist hierbei
     *              die Notiz-Id
     */
    private void fuegeFotosHinzu(@NonNull final Notiz notiz) {
        final Cursor fotoCursor = db.getReadableDatabase().query(
                FotoTabelle.TABELLEN_NAME,
                new String[]{FotoTabelle.FOTO_ID, FotoTabelle.NAME, FotoTabelle.PFAD},
                FotoTabelle.FK_FILTER,
                new String[]{String.valueOf(notiz.getNotizId())},
                null,
                null,
                null);
        try {
            // Cursor durchgehen und Werte auslesen
            if (fotoCursor.moveToFirst()) {
                do {
                    final Foto foto = new Foto(
                            fotoCursor.getInt(fotoCursor.getColumnIndex(IFotoSpalten.FOTO_ID)),
                            fotoCursor.getString(fotoCursor.getColumnIndex(IFotoSpalten.PFAD)),
                            fotoCursor.getString(fotoCursor.getColumnIndex(IFotoSpalten.NAME))
                    );
                    notiz.addFoto(foto);
                } while (fotoCursor.moveToNext());
            }
        } finally {
            fotoCursor.close();
        }
    }

    /**
     * Löscht eine Datei physisch vom System (Foto, Audio)
     *
     * @param dateipfad Pfad der Datei.
     */
    private void loescheDatei(@NonNull final String dateipfad) {
        File datei = new File(dateipfad);
        if (datei.exists()) {
            datei.delete();
        }
    }
}
