/*
 * Zweck:
 * Adapter für Anzeige der Verfügbaren Audiodateien und Abspielfunktion
 *
 * Author/Version/Datum:
 * mak/v1/04.6. Gerüst erstellt & Funktionalität implementiert
 * mak/v2/05.6. Klasse kommentiert
 **/
package ch.ffhs.amhs.notesatmobile.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import ch.ffhs.amhs.notesatmobile.R;
import ch.ffhs.amhs.notesatmobile.data.Audio;
import ch.ffhs.amhs.notesatmobile.data.PersistenzHelper;

/**
 * Adapter für Audioanzeige plus Abspielfunktion innerhalb der Anhänge
 */
public class AudioPagerAdapter extends PagerAdapter implements MediaPlayer.OnCompletionListener {

    private final Context context;
    private final LayoutInflater inflater;
    private final List<Audio> audioList;
    private final Runnable deleteDelegate;

    private MediaPlayer mediaPlayer;
    private FileInputStream audioInputStream;

    private ImageView activeImage;
    private boolean play = false;


    /**
     * Konstruktor
     * @param context Kontext
     * @param audioList Liste aller Audio-Dateien
     * @param deleteDelegate Delegate-Methode für das löschen
     */
    public AudioPagerAdapter(@NonNull final Context context, @NonNull final List<Audio> audioList, @NonNull final Runnable deleteDelegate) {
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.audioList = audioList;
        this.deleteDelegate = deleteDelegate;
    }

    /**
     * Gibt die Anzahl Listeneinträge zurück
     * @return
     */
    @Override
    public int getCount() {
        return audioList.size();
    }

    /**
     * Fragt, ob die View vom Objekt ist
     * @param view
     * @param object
     * @return
     */
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    /**
     * Instanzierungs-Funktion
     * @param container Container
     * @param position Position des Elements
     * @return Gibt das Item zurück
     */
    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        View itemView = inflater.inflate(R.layout.pager_item, container, false);
        final ImageView imageView = (ImageView) itemView.findViewById(R.id.fotoView);
        imageView.setImageResource(R.drawable.play);

        // Listener für Abspielen / Anhalten
        imageView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                activeImage = imageView;
                if (play) {
                    // change icon
                    imageView.setImageResource(R.drawable.play);
                    stopAudioAbspielen();
                    play = false;
                } else {
                    imageView.setImageResource(R.drawable.pause);
                    startAudioAbspielen(audioList.get(position).getPfad());
                    play = true;
                }
            }
        });

        // Listener für Context Menü eines Bildes
        imageView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                new AlertDialog.Builder(context)
                        .setTitle(R.string.TitelLoeschenBestaetigen)
                        .setMessage(R.string.TitelLoeschenBestaetigen)
                        .setPositiveButton(R.string.OptionJa, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                PersistenzHelper.getInstance(context).loescheAudio(
                                        audioList.get(position)
                                );
                                // View Updaten
                                deleteDelegate.run();
                            }
                        })
                        .setNegativeButton(R.string.OptionNein, null).show();
                return true;
            }
        });

        container.addView(itemView);
        return itemView;
    }

    /**
     * Startet das Abspielen der Audio-Datei
     * @param path Pfad der Audiodatei
     */
    private void startAudioAbspielen(@NonNull final String path) {
        mediaPlayer = new MediaPlayer();
        try {
            File file = new File(path);
            audioInputStream  = new FileInputStream(file);
            mediaPlayer.setOnCompletionListener(this);
            mediaPlayer.setDataSource(audioInputStream.getFD());
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Stoppt das Abspielen der Audiodatei
     */
    private void stopAudioAbspielen() {
        mediaPlayer.stop();
        mediaPlayer.reset();
        mediaPlayer.release();
        try {
            audioInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Destroy-Funktion
     * @param container Container
     * @param position Position
     * @param object Objekt
     */
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

    /**
     * OnCompletition Methode, welche das Abspielen des Sounds stoppt und resetted
     * @param mp MediaPlayer
     */
    @Override
    public void onCompletion(MediaPlayer mp) {
        stopAudioAbspielen();
        activeImage.setImageResource(R.drawable.play);
        play = false;
    }
}
