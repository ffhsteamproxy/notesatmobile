/*
 * Zweck:
 * Tabellenabbildung für die Stichworte
 *
 * Author/Version/Datum:
 * hos/v1/15.5. Grundgerüst
 * hos/v2/16.5. SQL Statements
 * hos/v3/22.5. FK, PK Filter
 */

package ch.ffhs.amhs.notesatmobile.db;

/**
 * Umsetzung der Tabelle für Stichworte
 */
public final class StichwortTabelle implements IStichwortSpalten {

    /** Tabellenname */
    public static final String TABELLEN_NAME = "Stichwort";


    /**
     *  Scripts - Tabellen erstellen, löschen
     *  -> einen Drop braucht es nicht, da wir keinen Update machen
     */
    public static final String SQL_CREATE = "" +
            "CREATE TABLE " + TABELLEN_NAME + " (" +
            NOTIZ_ID + " INT NOT NULL," +
            STICHWORT_TEXT + " TEXT NOT NULL," +
            "CONSTRAINT PFStichwortId PRIMARY KEY (" + NOTIZ_ID + "," + STICHWORT_TEXT + ")," +
            "CONSTRAINT FKStichwortNotizId FOREIGN KEY (" + NOTIZ_ID +") "
                + "REFERENCES " + NotizTabelle.TABELLEN_NAME + " (" + NotizTabelle.NOTIZ_ID +"));";

    /**
     * CRUD - Where Clause für Primary Key
     */
    public static final String PK_FILTER = "" + // Löschen eines Stichwort
            NOTIZ_ID + " = ? " +
            " AND " + STICHWORT_TEXT + " = ?;";

    /**
     * CRUD - Where Clause für Notiz Foreign Key, Massen-Löschen
     */
    public static final String FK_FILTER = "" + // Löschen aller Stichworte
            NOTIZ_ID + " = ?;";

}
