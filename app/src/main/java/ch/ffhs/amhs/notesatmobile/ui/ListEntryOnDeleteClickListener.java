/*
 * Zweck:
 * Listener für Klick-Aktionen auf den Löschen-Button eines Entries
 *
 * Author/Version/Datum:
 * mak/v1/28.5. Gerüst erstellt & Funktionalität implementiert
 * mak/v2/29.5. Klasse kommentiert
 */

package ch.ffhs.amhs.notesatmobile.ui;

import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.ViewGroup;

import ch.ffhs.amhs.notesatmobile.R;
import ch.ffhs.amhs.notesatmobile.data.PersistenzHelper;

/**
 * OnClickListener für Aktionen auf den Delete-Button
 */
class ListEntryOnDeleteClickListener extends BaseListEntryOnClickListener {

    private final Runnable delegate;

    /**
     * Konstruktor
     * @param parent Parent-Instanz
     * @param entryValue EntryValue Objekt mit Informationen über das Element
     */
    ListEntryOnDeleteClickListener(@NonNull final ViewGroup parent,
                                   @NonNull final ListViewEntryValue entryValue,
                                   @NonNull final Runnable delegate) {
        super(parent, entryValue);
        this.delegate = delegate;
    }

    /**
     * OnClick Aktion bei Klicken des Buttons
     * Es wird ein Dialog angezeigt, welcher nachfragt, ob eine Notiz gelöscht werden soll.
     * Ein Tippen auf Ja bestätigt den Löschvorgang und löscht die Notiz, ein Nein beendet den
     * Dialog wieder.
     * @param v View
     */
    @Override
    public void onClick(View v) {
        new AlertDialog.Builder(getParent().getContext())
                .setTitle(R.string.TitelLoeschenBestaetigen)
                .setMessage(R.string.TitelLoeschenBestaetigen)
                .setPositiveButton(R.string.OptionJa, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        PersistenzHelper.getInstance(getParent().getContext()).loescheNotizFromId(
                                getEntryValue().getNotizId());
                        delegate.run();
                    }
                })
                .setNegativeButton(R.string.OptionNein, null).show();
    }
}
