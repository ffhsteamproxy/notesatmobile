/*
 * Zweck:
 * Interface für einen Audio-Anhang einer Notiz
 *
 * Author/Version/Datum:
 * mak/v1/13.5. Grundgerüst
 */

package ch.ffhs.amhs.notesatmobile.lib;

public interface IAudioanhang extends IAnhang {
}
