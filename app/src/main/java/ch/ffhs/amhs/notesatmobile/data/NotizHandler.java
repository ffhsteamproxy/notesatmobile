/*
 * Zweck:
 * Erestllung einer Notiz mit allen Daten, die verlangt sind
 *
 * Author/Version/Datum:
 * hos/v1/28.5. Basisfunktionen
 * hos/v2/31.5. Nur Pre-Save Funktionalitäten
 *
 */

package ch.ffhs.amhs.notesatmobile.data;

import java.sql.Timestamp;

/**
 * Klasse zum Erstellen neuer Notizen
 */
public class NotizHandler {

    /**
     * Methode mit neuer, initialisierten Notiz
     * @return Notiz, die alle relevanten Werte gesetzt hat
     */
    public static Notiz erstelleNotiz(){

        // Erstellen der Notiz
        // Die GPS DatenGPS Daten aus dem Service holen
        return new Notiz();
    }

    /**
     * Methode um ein neues Notiz Proxy zu erstellen
     * @param notizId Id der Npotiz
     * @return neues Proxy einer Notiz
     */
    public static Notiz erstelleNotizProxy(Long notizId){
        final Notiz proxy = erstelleNotiz();
        proxy.setNotizId(notizId);
        return proxy;
    }

    /**
     * Hilfsmethode zum Anpassen der Daten einer Notiz vor dem Speichern
     * @param notiz
     */
    static void preSafeUpdate(Notiz notiz){

        // Aktueller Zeitpunkt
        Timestamp now = new Timestamp(System.currentTimeMillis());

        // Anpassen der Daten
        if (notiz.getSpeicherungszeitpunkt() == null)
            notiz.setSpeicherungszeitpunkt(now);

        notiz.setBearbeitetZeitpunkt(now);
    }
}
