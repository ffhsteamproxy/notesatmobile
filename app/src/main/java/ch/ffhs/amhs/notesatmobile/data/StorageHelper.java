/* Zweck:
 * Transportobjekt für ein Audio
 *
 * Author/Version/Datum:
 * mak/v1/20.5.  Grundgerüst
 */
package ch.ffhs.amhs.notesatmobile.data;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;

import java.io.File;
import java.util.UUID;

/**
 * Helperklasse für Speicherfunktionalität von Fotos
 */
public class StorageHelper {

    private Context context;
    private boolean externalStorageAvailable = false;
    private boolean externalStorageWriteable = false;

    /**
     * Konstruktor des Helpers
     * @param context Kontext / Activity
     */
    public StorageHelper(Context context) {
        this.context = context;
        init();
    }

    /**
     * Intialisierungsmethode
     */
    void init() {
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // Media ist eingehängt, lesbar und schreibbar
            externalStorageAvailable = externalStorageWriteable = true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // Media ist nur lesbar
            externalStorageAvailable = true;
            externalStorageWriteable = false;
        } else {
            // Nicht lesbar / Schreibbar oder nicht reingehängt
            externalStorageAvailable = externalStorageWriteable = false;
        }
    }

    /**
     * Hilfsmethode, welche einen eindeutigen Dateiname mit gegebenen Postfix erzeugt
     * @param postfix Postfix (z.B. jpg, 3ggp)
     * @return Verwendbaren Dateinamen
     */
    public static String createUniquueFileName(@NonNull final String postfix) {
        return UUID.randomUUID().toString().concat(".").concat(postfix);
    }

    /**
     * Gibt zurück, ob der Externe Storage verfügbar ist
     * @return true wenn verfügbar, false wenn nicht
     */
    public boolean isExternalStorageAvailable() {
        return externalStorageAvailable;
    }

    /**
     * Gibt zurück ob der Externe Storage schreibbar ist
     * @return true wenn schreibbar, false wenn nicht
     */
    public boolean isExternalStorageWriteable() {
        return externalStorageWriteable;
    }

    /**
     * Erstellt eine Speicher-URI für ein neues Bild. Diese Methode soll aus einem Intent
     * für die Kamera mitgegeben werden
     * @param filename Dateiname für Speicherung
     * @return Gibt eine Uri zurück, welche der Intent verwenden kann
     */
    @Nullable
    public Uri createImageDestinationUri(String filename) {
        // Null zurückgeben, wenn der Externe speicher nicht verfügbar ist
        if (!this.isExternalStorageAvailable()) {
            return null;
        }
        // Null zurückgeben, wenn der externe Speicher nicht schreibbar ist.
        if (!this.isExternalStorageWriteable()) {
            return null;
        }

        String path = Environment.DIRECTORY_PICTURES;
        // Externer speicherort aus dem Context holen
        File picturePath = context.getExternalFilesDir(path);
        if (picturePath == null) {
            // wenn er nicht aus dem Context geholt werden kann, soll er aus der Environment
            // geholt werden
            picturePath = Environment.getExternalStoragePublicDirectory( path );
        }
        // File erstellen
        File file = new File(picturePath, filename );
        // Uri erstellen und zurückgeben
        return FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", file);
    }
}
