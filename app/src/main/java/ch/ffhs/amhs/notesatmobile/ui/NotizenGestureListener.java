/*
 * Zweck:
 * Listener für Gesten-Steuerung
 *
 * Author/Version/Datum:
 * mak/v1/26.5. Listener erstellt
 * mak/v2/29.5. Klasse kommentiert
 */

package ch.ffhs.amhs.notesatmobile.ui;

import android.support.annotation.NonNull;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ViewSwitcher;

import ch.ffhs.amhs.notesatmobile.R;

/**
 * Listener für Gesten auf einer ListView
 */
public class NotizenGestureListener extends GestureDetector.SimpleOnGestureListener {

    private final ListView list;

    /**
     * Konstruktor
     * @param list ListView der Einträge
     */
    public NotizenGestureListener(@NonNull final ListView list) {
        this.list = list;
    }

    /**
     * OnFling Methode, welche die Gesten registiert und validiert
     * @param e1 Event 1
     * @param e2 Event 2
     * @param velocityX Velocity auf der X-Achse
     * @param velocityY Velocity auf der Y-Achse
     * @return true, wenn der Event zutrifft, false wenn nicht
     */
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return (showDeleteButton(e1)) || super.onFling(e1, e2, velocityX, velocityY);
    }

    /**
     * Prüft, ob der Delete-Button anhand des Events gezeigt werden soll.
     * @param e1 MouseEvent 1
     * @return true wenn ja / false wenn nein
     */
    private boolean showDeleteButton(MotionEvent e1) {
        int pos = list.pointToPosition((int) e1.getX(), (int) e1.getY());
        return showDeleteButton(pos);
    }

    /**
     * Prüft, ob der Delete-Button anhand der Position gezeigt werden soll.
     * @param pos Position des Events
     * @return true wenn ja / false wenn nein
     */
    private boolean showDeleteButton(int pos) {
        View child = list.getChildAt(pos);
        if (child != null) {
            ImageButton delete = (ImageButton) child.findViewById(R.id.entry_loesche_notiz);
            ViewSwitcher viewSwitcher = (ViewSwitcher) child.findViewById(R.id.viewswitcher);

            if (delete != null) {
                viewSwitcher.showNext();
            }
            return true;
        }
        return false;
    }


}
