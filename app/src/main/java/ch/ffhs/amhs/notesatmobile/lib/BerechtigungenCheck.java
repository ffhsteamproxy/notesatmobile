/*
 * Zweck:
 * Berechtigungsklassen
 *
 * hos/v1/4.6. Berechtigungen für GPS und SMS
 * mak/v2/5.6. Berechtigungen für STORAGE, CONTACTS
 */

package ch.ffhs.amhs.notesatmobile.lib;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

/**
 * Berechtigungen Hilfsklasse
 */
public class BerechtigungenCheck {

    /**
     * Check auf die für die App für GPS notwendigen Berechtigungen
     */
    public static void checkBerechtigung(Activity activity) {
        // Wenn es keine Berechtigung gibt ...
        if (!(isBerechtigtFuerGPS(activity) && isBerechtigtFuerSMS(activity) && isBerechtigtFuerAufnahme(activity)
            && isBerechtigtFuerStorageLesen(activity) && isBerechtigtFuerStorageSchreiben(activity)
                && isBerechtigtFuerLeseKontakte(activity))) {

            // ...dann nachfragen
            ActivityCompat.requestPermissions(activity,
                    new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.SEND_SMS,
                            Manifest.permission.RECORD_AUDIO,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.INTERNET,
                            Manifest.permission.READ_CONTACTS
                    },
                    PackageManager.PERMISSION_GRANTED);
        }
    }

    /**
     * Hilfsmethode: prüft die Zustimmung der Berechtigung für GPS
     */
    public static boolean isBerechtigtFuerGPS(Context context) {
        return (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED);
    }

    /**
     * Hilfsmethode: prüft die Zustimmung der Berechtigung für GPS
     */
    public static boolean isBerechtigtFuerSMS(Context context) {
        return (ContextCompat.checkSelfPermission(context, Manifest.permission.SEND_SMS)
                == PackageManager.PERMISSION_GRANTED);
    }

    /**
     * Hilfsmethode: prüft die Zustimmung der Berechtigung
     */
    public static boolean isBerechtigtFuerAufnahme(Context context) {
        return (ContextCompat.checkSelfPermission(context, Manifest.permission.RECORD_AUDIO)
                == PackageManager.PERMISSION_GRANTED);
    }

    /**
     * Hilfsmethode: prüft die Zustimmung der Berechtigung für Lesen des externen Storages
     */
    public static boolean isBerechtigtFuerStorageLesen(@NonNull final Context context) {
        return (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED);
    }

    /**
     * Hilfsmethode: prüft die Zustimmung der Berechtigung für Schreiben des externen Storages
     */
    public static boolean isBerechtigtFuerStorageSchreiben(@NonNull final Context context) {
        return (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED);
    }

    public static boolean isBerechtigtFuerLeseKontakte(@NonNull final Context context) {
        return (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_CONTACTS)
                == PackageManager.PERMISSION_GRANTED);
    }
}
