/*
 * Zweck:
 * Transportobjekt für jedes Entry in der Hauptliste
 *
 * Author/Version/Datum:
 * mak/v1/28.5. Transportobjekt erstellt
 * mak/v2/29.5. Klasse kommentiert
 * mak/v3/03.6. Foto/Audio hinzugefügt
 */

package ch.ffhs.amhs.notesatmobile.ui;

import android.support.annotation.NonNull;

/**
 * Transportobjekt für Meta-Informationen eines ListView Records
 */
final class ListViewEntryValue {

    private final long notizId;
    private final String title;
    private final String datum;
    private final boolean hasFoto;
    private final boolean hasAudio;

    /**
     * Konstruktor
     * @param notizId Notiz-Id der aktuellen Notiz
     * @param title Titel der aktuellen Notiz
     * @param hasFoto Fotos vorhanden
     * @param hasAudio Audio vorhanden
     */
    ListViewEntryValue(final long notizId, @NonNull final String title, @NonNull final String datum, final boolean hasFoto, final boolean hasAudio) {
        this.notizId = notizId;
        this.title = title;
        this.datum = datum;
        this.hasFoto = hasFoto;
        this.hasAudio = hasAudio;
    }

    /**
     * Gibt die aktuelle NotizId zurück
     * @return NotizId als Long
     */
    long getNotizId() {
        return notizId;
    }

    /**
     * Gibt den Titel der aktuellen Notiz zurück.
     * @return Titel der Notiz als String
     */
    @NonNull
    String getTitle() {
        return title;
    }

    /**
     * Gibt das Datum der aktuellen Notiz zurück.
     * @return Datum der Notiz als String
     */
    @NonNull
    String getDatum() { return datum;  }

    /**
     * Gibt zurück, ob Fotos vorhanden sind
     * @return
     */
    boolean hasFoto() {
        return hasFoto;
    }

    /**
     * Gibt zurück ob Audios vorhanden sind
     * @return
     */
    boolean hasAudio() {
        return hasAudio;
    }
}
