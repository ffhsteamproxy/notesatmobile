/*
 * Zweck:
 * Anzeige für eine einzelne Notiz ohne Bearbeitungsmöglichkeiten
 * <p>
 * Author/Version/Datum:
 * hos/v1/8.4. Grundgerüst
 * hos/v2/24.4. Namensgebung
 * hos/v3/4.5. Namensgebung
 * hos/v4/21.5. Anzeige (vorerst mit Mock-Daten)
 * hos/v5/26.5. Zurück Button
 * hos/v6/28.5. Löschen Button
 * hos/v7/5.6. Rückfrage löschen
 * <p>
 * TODO, Anzeige des Änderungsdatums, weshalb sollten wir es sonst speichern?
 * TODO, Fotos und Anhänge
 */

package ch.ffhs.amhs.notesatmobile;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import ch.ffhs.amhs.notesatmobile.data.Notiz;
import ch.ffhs.amhs.notesatmobile.data.PersistenzHelper;
import ch.ffhs.amhs.notesatmobile.lib.BerechtigungenCheck;
import ch.ffhs.amhs.notesatmobile.lib.NotesConstants;
import ch.ffhs.amhs.notesatmobile.services.INotizSendenRunnable;
import ch.ffhs.amhs.notesatmobile.services.NotizSendenService;
import ch.ffhs.amhs.notesatmobile.services.NotizSendenServiceConnection;
import ch.ffhs.amhs.notesatmobile.ui.AnzeigeHelper;

/**
 * Activity-Klasse für die Anzeige einer Notiz
 */
public class NotizAnzeigeActivity extends AppCompatActivity {

    /**
     * Notizdaten für die Anzeige
     */
    private Notiz notiz;

    public static final int ID_CONTACT = 2;

    /**
     * On Create einer Activity,
     * Fenster öffnen und initialisieren
     *
     * @param savedInstanceState Bundle mit übergebenen Daten
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // Öffnen des Fensters
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notiz_anzeige);

        // Daten anzeigen
        // TODO Test, wenn die Activity gekillt wird ... onPostPause oder so, ID Speichern?
        setNotizFromBundle(getIntent().getExtras());
        zeigeNotiz();
    }

    /**
     * Löschen Aufruf aus der Activity
     *
     * @param view aus der Activity übergebene View
     */
    public void onNotizLoeschen(View view) {

        // Nachfrage
        new AlertDialog.Builder(this)
                .setTitle(R.string.TitelLoeschenBestaetigen)
                .setMessage(R.string.TitelLoeschenBestaetigen)
                .setPositiveButton(R.string.OptionJa, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Wenn ja, dann löschen
                        loescheNotiz();
                        finish(); // Fenster wieder schliessen
                    }
                })
                .setNegativeButton(R.string.OptionNein, null).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ID_CONTACT) {
            if (resultCode == Activity.RESULT_OK) {
                Uri contactData = data.getData();
                final Cursor c = getContentResolver().query(contactData, null, null, null, null);
                if (c.moveToFirst()) {
                    final String number = c.getString(c.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    sendeSMS(number);
                }
                c.close();
            }
        }
    }

    /**
     * Notiz zum Bearbeiten öffnen
     *
     * @param view vom GUI übergebene View
     */
    public void onNotizBearbeiten(View view) {

        // Bundle erstellen
        final Bundle bundle = new Bundle();
        AnzeigeHelper.addNotizToBundle(notiz, bundle);

        // Intent erstellen und Activity staretn
        final Intent intent = new Intent(this, NotizBearbeitungActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();
    }

    public void onNotizTeilen(View view) {
        if (BerechtigungenCheck.isBerechtigtFuerSMS(this)) {
            final Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
            startActivityForResult(intent, ID_CONTACT);
        }
    }

    /**
     * Aktion nach dem Button-Click "Anlagen"
     * @param view vom GUI übergebene View
     */
    public void onAnlagenAnzeigen(View view) {
        // Bundle erstellen
        final Bundle bundle = new Bundle();
        AnzeigeHelper.addNotizToBundle(notiz, bundle);
        bundle.putBoolean(AnhangActivity.FLAG_EDIT_MODE, false);

        // Intent erstellen und Activity staretn
        final Intent intent = new Intent(this, AnhangActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    /**
     * In der Maske zurück navigieren
     *
     * @param view aus dem GUI übergebene View
     */
    public void onZurueckNavigieren(View view) {
        finish(); // Die Anzeige kann ohne weitere Info's gelösch werden.
    }

    /**
     * Notiz Versenden-Bestätigenä-Dialog
     *
     * @return Antwort aus dem Dialog
     */
    private boolean sendenBestaetigen() {
        // TODO
        return true;
    }

    /**
     * Alle Daten der Notiz auf dem GUI anzeigen
     */
    private void zeigeNotiz() {

        // Titel
        final TextView itemTitel = (TextView) findViewById(R.id.ToolbarAnzeigenTitle);
        itemTitel.setText(notiz.getTitel());

        // Anzeige der Notiz (Text-)Daten
        final TextView itemText = (TextView) findViewById(R.id.FeldNotizId);
        itemText.setText(notiz.getDetails());

        // Datum
        if (notiz.getSpeicherungszeitpunkt() != null) { // eigentlich sollte das nicht passieren, aber die Option braucht es trotzdem

            String datumZeitText;

            // Text für die Anzeige bestimmen, abhängig davon, wie oft gespeichert wurde
            if (notiz.getSpeicherungszeitpunkt().equals(notiz.getVeraenderungszeitpunkt())) {
                datumZeitText = notiz.getSpeicherungszeitpunkt().toString(); // einmalig gespeichert
            } else { // mehrfaches speichern

                datumZeitText = getString(R.string.FeldDatumZeitText,
                        notiz.getSpeicherungszeitpunkt().toString(), notiz.getVeraenderungszeitpunkt().toString());
            }

            // Text anzeigen
            final TextView itemSpeicherzeitpunkt = (TextView) findViewById(R.id.FeldDatumZeitId);
            itemSpeicherzeitpunkt.setText(datumZeitText);
        }


        // Ort / in der Nähe von
        final String ortText;
        if (notiz.getOrt() == null) {
            ortText = getText(R.string.InhaltKeineGPSDaten).toString(); // Keine GPS Daten verfügbar
        } else {
            ortText = String.format(getText(R.string.TextInDerNaeheVon).toString(), notiz.getOrt());
        }
        final TextView itemOrt = (TextView) findViewById(R.id.FeldOrtId);
        itemOrt.setText(ortText);

        // Stichworte
        final String stichworteText = AnzeigeHelper.anzeigeFuerStichworte(notiz.getStichworte());
        final TextView itemStichworte = (TextView) findViewById(R.id.FeldStichworteId);
        itemStichworte.setText(stichworteText);
    }

    /**
     * Notiz, die angezeigt wird, löschen
     */
    private void loescheNotiz() {
        PersistenzHelper.getInstance(this).loescheNotiz(notiz);

        // TODO, on Error show und nicht möglich, wenn nicht gespeichert.
    }

    /**
     * Hilfsmethode: Notizdaten aus den Informationen des Bundles laden
     *
     * @param bundle Aus dem Intent übergebenes Bundle
     */
    private void setNotizFromBundle(Bundle bundle) {
        notiz = AnzeigeHelper.getNotizFromBundle(bundle, this);
        //TODO Was wenn NULL?
    }

    /**
     * Methode, um eine SMS zu versenden
     * @param phoneNumber Telefonnummer des Empfängers
     */
    private void sendeSMS(@NonNull final String phoneNumber) {
        final ServiceConnection notizSendenService = new NotizSendenServiceConnection(new NotizListRunnable(this), notiz, phoneNumber); // fire and forget
        Log.d(NotesConstants.DEBUG_LOG_TAG, getString(R.string.MeldungStartNotizService));
        final Intent notizSendenIntent = new Intent(this, NotizSendenService.class);
        bindService(notizSendenIntent, notizSendenService, Context.BIND_AUTO_CREATE);
    }

    /**
     * Runnable Klasse für das Melden, dass die SMS versendet wurde und danach Beenden des Services
     */
    private class NotizListRunnable extends INotizSendenRunnable {

        private final Context context;

        public NotizListRunnable(@NonNull final Context context) {
            this.context = context;
        }

        public void run() {

            // Nachdem das Resultat bekannt ist, wird der Service beendet
            Log.d(NotesConstants.DEBUG_LOG_TAG, ergebnis);

            // Erste Antwort, unbind des Service
            if (ergebnis == NotizSendenService.SMS_SENDING) {
                // Nur für Debugging, um den Ablauf zu sehen
            } else { // weitere Antworten auf dem Display ausgeben

                if (ergebnis == NotizSendenService.SMS_SENT) {
                    Toast.makeText(context, context.getString(R.string.MeldungSMSVersendet, mobilNummer), Toast.LENGTH_LONG).show();


                } else {
                    Toast.makeText(context, R.string.ErrorSMSNichtVersendet, Toast.LENGTH_LONG).show();
                }
                connection.notizSendenServiceBinder.stopStatusUpdate();
                context.unbindService(connection); // der richtige Service befindet sich im Runnable
            }
        }
    }
}
