/*
 * Zweck:
 * Abstrakte Klasse für das Runnable für den Versand einer SMS
 * <p>
 * Author/Version/Datum:
 * hos/v1/4.6.
 */
package ch.ffhs.amhs.notesatmobile.services;

public abstract class INotizSendenRunnable implements Runnable {
    public String ergebnis;
    public String mobilNummer;
    public NotizSendenServiceConnection connection;
}