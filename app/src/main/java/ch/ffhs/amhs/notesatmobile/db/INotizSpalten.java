/*
 * Zweck:
 * Konstantenklasse für die Tabelle Notiz
 *
 * Author/Version/Datum:
 * mak/v1/13.5. Grundgerüst
 * hos/v2/13.5. Namensgebung
 * hos/v3/25.5. Beschreibung raus
 */
package ch.ffhs.amhs.notesatmobile.db;

/**
 * Interface Klasse für die Spalten der Tabelle Notiz
 */
public interface INotizSpalten {

    /**
     * Datenfelder
     */
    String NOTIZ_ID = "_id";
    String TITEL = "Titel";
    String DETAILS = "Details";
    String SPEICHERZEITPUNKT = "Speicherzeitpunkt";
    String AENDERUNGSZEITPUNKT = "Aenderungszeitpunkt";
    String LATITUDE = "Latitude";
    String LONGITUDE = "Longitude";
    String NAEHE = "Naehe";
}