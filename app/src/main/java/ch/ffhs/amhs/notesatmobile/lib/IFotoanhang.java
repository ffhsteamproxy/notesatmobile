/*
 * Zweck:
 * Interface für einen Foto-Anhang einer Notiz
 *
 * Author/Version/Datum:
 * mak/v1/13.5. Grundgerüst
 */

package ch.ffhs.amhs.notesatmobile.lib;

public interface IFotoanhang extends IAnhang {
}
