/*
 * Zweck:
 * Tabellen-View für die Liste der Notizen mit Audio/Foto
 *
 * Author/Version/Datum:
 * mak/v1/3.6. View erstellen
 * hos/v2/3.6. kleinere Korrekturen
 *
 */
package ch.ffhs.amhs.notesatmobile.db;

/**
 * View für die Suche der Notizen
 */
public class NotizView implements INotizSpalten {

    /**
     * Zugriffsparamater
     */
    public static final String VIEW_NAME = "NotizCursorView";
    public static final String COUNT_FOTO = "count_fotos";
    public static final String COUNT_AUDIO = "count_audio";

    /**
     * SQL Statement für die Erstellung der View
     */
    public static final String SQL_CREATE = "" +
            "CREATE VIEW " + VIEW_NAME + " AS SELECT " +
            "n." + NOTIZ_ID + " AS " + NOTIZ_ID + ", " +
            "n." + TITEL + " AS " + TITEL + ", " +
            "n." + DETAILS + " AS " + DETAILS + ", " +
            "n." + SPEICHERZEITPUNKT + " AS " + SPEICHERZEITPUNKT + ", " +
            "n." + AENDERUNGSZEITPUNKT + " AS " + AENDERUNGSZEITPUNKT + ", " +
            "n." + NAEHE + " AS " + NAEHE + ", " +
            "COUNT (f." + FotoTabelle.FOTO_ID + ") AS " + COUNT_FOTO + ", " +
            "COUNT (a." + AudioTabelle.AUDIO_ID + ") AS " + COUNT_AUDIO + " " +
            "FROM " + NotizTabelle.TABELLEN_NAME + " n " +
            "LEFT OUTER JOIN " + FotoTabelle.TABELLEN_NAME + " f ON (n." + NOTIZ_ID + " = " + "f." + FotoTabelle.NOTIZ_ID + ") " +
            "LEFT OUTER JOIN " + AudioTabelle.TABELLEN_NAME + " a ON (n." + NOTIZ_ID + " = " + "a." + AudioTabelle.NOTIZ_ID + ") " +
            "GROUP BY n." + NOTIZ_ID;

    /**
     * CRUD - Suche Where Statement
     * ANF.FU02.1 Suchen einer Notiz
     */
    public static final String SEARCH_FILTER = "" +
            TITEL + " LIKE ?" +
            " OR " + DETAILS + " LIKE ?" +
            " OR " + NAEHE + " LIKE ?" +
            // nach Datum und Uhrzeit des Erfassens bzw. der letzten Änderung zu suchen
            " OR " + SPEICHERZEITPUNKT + " LIKE ?" +
            " OR " + AENDERUNGSZEITPUNKT + " LIKE ?" +
            " OR " + NOTIZ_ID + " IN (" +
            "SELECT " + StichwortTabelle.NOTIZ_ID +
            " FROM " + StichwortTabelle.TABELLEN_NAME +
            " WHERE " + StichwortTabelle.STICHWORT_TEXT + " LIKE ?" +
            ")";
}
