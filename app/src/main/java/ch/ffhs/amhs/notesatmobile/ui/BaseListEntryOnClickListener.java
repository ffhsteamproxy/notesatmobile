/*
 * Zweck:
 * Basisklasse für Listener für Klick-Aktionen
 *
 * Author/Version/Datum:
 * mak/v1/28.5. Gerüst erstellt & Funktionalität implementiert
 * mak/v2/29.5. Klasse kommentiert
 */

package ch.ffhs.amhs.notesatmobile.ui;

import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
/**
 * Basisklasse für OnClickListener von Buttons, welche beim Swipe aus der ListView angezeigt
 * werden.
 */
abstract class BaseListEntryOnClickListener implements View.OnClickListener {

    private final ListViewEntryValue entryValue;
    private final ViewGroup parent;

    /**
     * Konstruktor
     * @param parent Parent-Instanz
     * @param entryValue EntryValue Objekt mit Informationen über das Element
     */
    public BaseListEntryOnClickListener(@NonNull final ViewGroup parent,
                                        @NonNull final ListViewEntryValue entryValue) {
        this.parent = parent;
        this.entryValue = entryValue;
    }

    /**
     * Gibt die Parent-Instanz zurück
     * @return ParentInstanz als ViewGroup
     */
    @NonNull
    protected ViewGroup getParent() {
        return parent;
    }

    /**
     * Gibt das EntryValue mit den Informationen zurück
     * @return ListViewEntryValue mit Titel und NotizId
     */
    @NonNull
    protected ListViewEntryValue getEntryValue() {
        return entryValue;
    }
}
