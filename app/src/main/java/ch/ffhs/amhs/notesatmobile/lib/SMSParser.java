/*
 * Zweck:
 * Konverter-Klasse Notiz -> SMS -> Notiz
 *
 * Author/Version/Datum:
 * mak/v1/13.5. Grundgerüst
 * mak/v2/15.5. parseNotiz hinzugefügt
 * hos/v3/21.5. erstelleSMS, encode/decode Array, String
 * hos/v4/28.5. Transfer Schlüssel
 *
 * TODO, encode/decode für den Delimiter festlegen -> wir könnten auch mit Start-Tags arbeiten
 * START_TAG_NOTIZ = "Notiz:", ich denke das wäre sicherer!!!
 *
 * TODO, die Stichworte kommen eigentlich immer, was tun?
 * TODO, weitere Daten (Datum, GPS?)
 */


package ch.ffhs.amhs.notesatmobile.lib;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import java.util.List;

import ch.ffhs.amhs.notesatmobile.data.Notiz;
import ch.ffhs.amhs.notesatmobile.data.NotizHandler;

/**
 * Parser und Generator für SMS Nachrichten
 */
public class SMSParser {

    /** Konstanten mit Zeichen für Encode/Decoden */
    private static final String TRANSFER_DELIMITER = ";"; // Export/Import Delimiter
    private static final String ARRAY_OPEN_IDENTIFIER = "("; // Array to String Open
    private static final String ARRAY_CLOSE_IDENTIFIER = "("; // Array to String Close
    private static final String ARRAY_DELIMITER = ","; // Array to String Open

    /** Schlüssel für den Austausch von Nachrichten */
    public static final String TRANSFER_KEY = "NOTESATMOBILE_10";

    /**
     * Methode zum Parsen einer Notiz (ohne Anhang) aus dem Text einer SMS
     * @param smsText Text aus der SMS
     * @return Notiz mit den Daten der ASMS
     */
    public static Notiz parseNotiz(@NonNull final String smsText) {
        final Notiz notiz = NotizHandler.erstelleNotiz();
        String[] values = smsText.split(TRANSFER_DELIMITER);
        boolean versionOk = false;
        boolean titleSet = false;
        boolean textSet = false;
        boolean keywordSet = false;


        for (String s : values) {

            // Versionscheck
            if(!versionOk){
                if (s.equals(TRANSFER_KEY)) {
                    versionOk = true;
                } else {
                    return null;
                }
                continue;
            }

            // Titel setzen
            if (!titleSet) {
                notiz.setTitel(decodeText(s));
                titleSet = true;
                continue;
            }

            // Text setzen
            if (!textSet) {
                notiz.setDetails(decodeText(s));
                textSet = true;
                continue;
            }

            // Stichworte Setzen
            if (!keywordSet) {
                // Wieder separieren

                final String[] keywords = decodeArray(s);
                for (String kw : keywords) {
                    notiz.addStichwort(kw.trim());
                }
                // TODO, theoretisch kommen die immer, wenn ich die immer als () encode
                keywordSet = true;
            }
        }
        return notiz;
    }

    /**
     * Schreiben einer Notiz in einen Text um diesen mit Hilfe von SMS auszutauschen
     * @param notiz Notiz, die in eine SMS übergeben wird
     * @return der Text für die SMS
     */
    //
    public static String erstelleSMS(@NonNull final Notiz notiz) {

        final StringBuilder smsText = new StringBuilder();

        // Schlüssel und Delimiter
        smsText.append(TRANSFER_KEY);
        smsText.append(TRANSFER_DELIMITER);

        // Standard-Daten hinzufügen
        smsText.append(encodeText(notiz.getTitel()));
        smsText.append(TRANSFER_DELIMITER);
        smsText.append(encodeText(notiz.getDetails())
        );
        smsText.append(TRANSFER_DELIMITER);
        smsText.append(encodeArray(notiz.getStichworte()));
        smsText.append(TRANSFER_DELIMITER);

        // TODO Datum und GPS Daten

        return smsText.toString();
    }

    /**
     * Hilfsmethode: Encode eines Strings, sodass Delimiter nicht drin landen
     * @param text Text aus einer Notiz-Variable
     * @return Codierter Text für die SMS
     */
    private static String encodeText(String text){
        // TODO, Delimiter muss encoded werden oder ähnlich
        return text;
    }

    /**
     * Hilfsmethode: Decode eines versandten Strings
     * @param text Teil-Text aus einer SMS
     * @return Decodierter Text für die Notiz-Variable
     */
    private static String decodeText(String text){
        // TODO, Gegenmethode zu oben
        return text;
    }

    /**
     * Hilfsmethode: Encode einer Liste für die SMS
     * @param array Liste aus Strings aus einer Notiz-Variable
     * @return Codierter Text für die SMS
     */
    private static String encodeArray(List<String> array){

        final StringBuilder arrayText = new StringBuilder();

        // Array - Text definieren
        arrayText.append(ARRAY_OPEN_IDENTIFIER);
        arrayText.append(TextUtils.join(ARRAY_DELIMITER, array));
        arrayText.append(ARRAY_CLOSE_IDENTIFIER);

        return arrayText.toString();
    }

    /**
     * Hilfsmethode: Decode eines versandten Strings
     * @param text Teil-Text aus einer SMS
     * @return Decodierte Liste für die Notiz-Variable
     */
    private static String[] decodeArray(String text){
        // Klammern werden aus dem Code entfernt
        String keyWordCommaList = text.replace(ARRAY_OPEN_IDENTIFIER, NotesConstants.EMPTY_STRING)
                .replace(ARRAY_OPEN_IDENTIFIER, NotesConstants.EMPTY_STRING);
        // Text splitten und zurückgeben
        return keyWordCommaList.split(ARRAY_DELIMITER);
    }

    // Encode eines Datums
}