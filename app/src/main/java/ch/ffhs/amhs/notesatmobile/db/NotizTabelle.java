/*
 * Zweck:
 * Tabellenabbildung für eine Notiz
 *
 * Author/Version/Datum:
 * hos/v1/15.5. Grundgerüst
 * hos/v2/16.4. SQL Statements
 * hos/v3/25.5. Beschreibung raus
 * hos/v4/2.6. double für die Längen und Breitengrade
 */

package ch.ffhs.amhs.notesatmobile.db;

// SQLite kennt kein Date/Time, speichern als TEXT -> YYYY-MM-DD HH:MM:SS.SSS
// Wie wird das Datum bei der Suche gehandelt?

/**
 * Umsetzung der Tabelle für eine Notiz
 */
public final class NotizTabelle implements INotizSpalten {

    /** Tabellenname */
    public static final String TABELLEN_NAME = "Notiz";

    /**
     *  Scripts - Tabellen erstellen, löschen
     *  -> einen Drop braucht es nicht, da wir keinen Update machen
     */
    public static final String SQL_CREATE = "" +
            "CREATE TABLE " + TABELLEN_NAME + " (" +
            NOTIZ_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
            TITEL + " TEXT NOT NULL," +
            DETAILS  + " TEXT," +
            SPEICHERZEITPUNKT + " TEXT NOT NULL," + // SQLite kennt kein Date/Time
            AENDERUNGSZEITPUNKT + " TEXT NOT NULL," + // SQLite kennt kein Date/Time
            LATITUDE + " REAL," + // der GSP Empfaenger muss keine Daten liefern
            LONGITUDE + " REAL," +
            NAEHE + " TEXT);"; // Name der Ortschaft in der Nähe

    /**
     * CRUD - Where Clause für Primary Key
     */
    public static final String PK_FILTER = "" + // Löschen eines Stichwort
            NOTIZ_ID + " = ? ";


    // TODO, um Parameter zu sparen könnte man Texte mergen
    // "CONCAT(" + TITEL +"," + BESCHREIBUNG + "," + NAEHE + ") LIKE" ...

}
