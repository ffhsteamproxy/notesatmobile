/*
 * Zweck:
 * Anzeige für Anhänge (Fotos und Audio)
 *
 * Author/Version/Datum:
 * mak/v1/4.6. Grundgerüst & Funktionalität
 * hos/v2/5.6. Zurück Button
 * mak/v3/05.6. Berechtigungschecks gemoved.
 */
package ch.ffhs.amhs.notesatmobile;

import android.content.Intent;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import ch.ffhs.amhs.notesatmobile.data.Audio;
import ch.ffhs.amhs.notesatmobile.data.Foto;
import ch.ffhs.amhs.notesatmobile.data.Notiz;
import ch.ffhs.amhs.notesatmobile.data.NotizHandler;
import ch.ffhs.amhs.notesatmobile.data.PersistenzHelper;
import ch.ffhs.amhs.notesatmobile.data.StorageHelper;
import ch.ffhs.amhs.notesatmobile.lib.BerechtigungenCheck;
import ch.ffhs.amhs.notesatmobile.ui.AnzeigeHelper;
import ch.ffhs.amhs.notesatmobile.ui.AudioPagerAdapter;
import ch.ffhs.amhs.notesatmobile.ui.FotoPagerAdapter;

/**
 * Activity-Klasse für Anhänge
 */
public class AnhangActivity extends AppCompatActivity {


    private static final int CAPTURE_IMAGE= 1;

    public static final String FLAG_EDIT_MODE = "flag_edit_mode";
    private static final String KEY_CAPTURE_IMAGE_URI = "KEY_CAPTURE_IMAGE_URI";

    private Notiz notiz;
    private ViewPager fotoViewPager;
    private ViewPager audioViewPager;
    private Button btnFotoHinzufuegen;
    private Button btnAudioHinzufuegen;
    private boolean isEditMode = false;
    private boolean aufnahmeActive = false;

    private Uri savedImageUri = null;
    private String savedRecordPath = null;

    private MediaRecorder mMediaRecorder;
    private FileOutputStream audioAufnahmeStream;

    /**
     * On Create einer Activity,
     * Fenster öffnen und initialisieren
     * @param savedInstanceState Bundle mit übergebenen Daten
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anhang);
        if (isBerechtigtFuerSpeichernUndAufnahme()) {
            isEditMode = getIntent().getExtras().getBoolean(FLAG_EDIT_MODE);
            setNotizFromBundle(getIntent().getExtras());
            init();
        } else {
            finish();
        }
    }

    /**
     * Resume-Methode für Aktionen nach dem Aufnehmen eines Fotos
     */
    @Override
    protected void onResume() {
        super.onResume();
        if (savedImageUri != null) {
            // Bild speichern
            Foto foto = new Foto(0, savedImageUri.toString(), "");
            // Notiz auf der DB speichern
            foto = PersistenzHelper.getInstance(this).speichereFoto(foto, notiz.getNotizId());
            notiz.addFoto(foto);
            savedImageUri = null;
            // Notiz mit Änderung speichern
            PersistenzHelper.getInstance(this).speichereNotiz(notiz);
            // Foto-View neu aufbauen
            fillFotoViewPager(notiz.getFotoAnhaenge());
        }
    }

    /**
     * Methode, um den Instanz-Status zu speichern
     * @param outState
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (savedImageUri != null) {
            outState.putString(KEY_CAPTURE_IMAGE_URI, savedImageUri.toString());
        }
    }

    /**
     * Hilfsmethode: Notizdaten aus den Informationen des Bundles laden.
     * Wird dem Bundle keine Notiz mitgegeben, so wird eine neue erstellt.
     *
     * @param bundle Aus dem Intent übergebenes Bundle
     */
    private void setNotizFromBundle(Bundle bundle) {

        if (AnzeigeHelper.hasNotizInBundle(bundle)) { // und gesetzter ID Wert
            notiz = AnzeigeHelper.getNotizFromBundle(bundle, this);
        } else {
            // Intent ungültig -> beenden
            finish();
        }
    }

    /**
     * Init-Methode, welches die UI-Elemente initialisiert und die Pager befüllt.
     */
    private void init() {

        // Titel
        final TextView itemTitel = (TextView) findViewById(R.id.ToolbarAnhangTitel);
        itemTitel.setText(notiz.getTitel());

        fotoViewPager = (ViewPager) findViewById(R.id.fotoViewPager);
        audioViewPager = (ViewPager) findViewById(R.id.audioViewPager);
        btnFotoHinzufuegen = (Button) findViewById(R.id.btnFotoHinzufuegen);
        btnAudioHinzufuegen = (Button) findViewById(R.id.btnAudioHinzufuegen);

        // Visibilität setzen (je nach Modus)
        int visibility = isEditMode ? View.VISIBLE : View.INVISIBLE;
        btnFotoHinzufuegen.setVisibility(visibility);
        btnAudioHinzufuegen.setVisibility(visibility);

        // Viewpager füllen
        fillFotoViewPager(notiz.getFotoAnhaenge());
        fillAudioViewPager(notiz.getAudioAnhaenge());
    }

    /**
     * Reload-Funktion, welche die Notiz neu aus der DB lädt und die View neu aufbaut.
     */
    private void reload() {
        final Notiz proxy = NotizHandler.erstelleNotiz();
        proxy.setNotizId(notiz.getNotizId());
        notiz = PersistenzHelper.getInstance(this).ladeNotiz(proxy);
        init();
    }

    /**
     * Klick-Aktion wenn auf den Button "Foto hinzufügen" geklickt wird.
     * @param view GUI-Element
     */
    public void onFotoHinzufuegenClick(View view) {
        StorageHelper helper = new StorageHelper(this);
        if (helper.isExternalStorageAvailable() && helper.isExternalStorageWriteable()) {
            String filename = StorageHelper.createUniquueFileName("jpg");

            Uri imageUri = helper.createImageDestinationUri(filename);
            savedImageUri = imageUri;

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            startActivityForResult(intent, CAPTURE_IMAGE);
        }
    }

    /**
     * Klick-Aktion wenn auf den Button "Aufnahme hinzufügen" geklickt wird.
     * @param view GUI Element
     */
    public void onAudioHinzufuegenClick(View view) {
        if (aufnahmeActive) {
            aufnahmeStoppen();
        } else {
            aufnahmeStarten();
        }
    }

    /**
     * Hilfsmethode, um den Foto-Pager zu füllen
     * @param fotos Liste aller Fotos
     */
    private void fillFotoViewPager(@NonNull final List<Foto> fotos) {
        // Ohne preload
        fotoViewPager.setOffscreenPageLimit(1);
        final FotoPagerAdapter fotoPager = new FotoPagerAdapter(this, fotos, new DeleteDelegate());
        fotoViewPager.setAdapter(fotoPager);
        initDotSwitches(fotos, fotoViewPager, fotoPager, R.id.fotoPagerDots);
    }

    /**
     * Hilfsmethode, um den Audio-Pager zu füllen
     * @param audio Liste aller Audios
     */
    private void fillAudioViewPager(@NonNull final List<Audio> audio) {
        // Ohne preload
        audioViewPager.setOffscreenPageLimit(1);
        final AudioPagerAdapter audioPager = new AudioPagerAdapter(this, audio, new DeleteDelegate());
        audioViewPager.setAdapter(audioPager);
        initDotSwitches(audio, audioViewPager, audioPager, R.id.audioPagerDots);
    }

    /**
     * Methode, um die Aufnahme einer Audiodatei zu starten.
     */
    private void aufnahmeStarten() {
        final String fileName = StorageHelper.createUniquueFileName("3gpp");

        // Aufnahme starten
        mMediaRecorder = new MediaRecorder();
        try {
            File file = new File(getExternalFilesDir(null), fileName);
            savedRecordPath = file.getPath();
            // Stream initialisieren
            audioAufnahmeStream = new FileOutputStream(file);
            // Sourcen, Output und Encoder setzen
            mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            mMediaRecorder.setOutputFile(audioAufnahmeStream.getFD());
            // Aufnahme starten
            mMediaRecorder.prepare();
            mMediaRecorder.start();
            aufnahmeActive = true;
            // Text auf Button ändern
            btnAudioHinzufuegen.setText(R.string.AudioAufnahmeStoppen);
            // Foto-Button deaktivieren
            btnFotoHinzufuegen.setEnabled(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Methode, um die Audioaufnahme zu stoppen
     */
    private void aufnahmeStoppen() {
        // Media-Recorder stoppen und releasen
        mMediaRecorder.stop();
        mMediaRecorder.reset();
        mMediaRecorder.release();
        // Aufnahmestatus auf false setzen
        aufnahmeActive = false;
        // Text auf Button neu setzen
        btnAudioHinzufuegen.setText(R.string.NeueAudio);
        try {
            // Stream schliessen
            audioAufnahmeStream.close();
            // Foto-Button wieder aktivieren
            btnFotoHinzufuegen.setEnabled(true);
            // Audio speichern
            Audio audio = new Audio(0, savedRecordPath, "");
            audio = PersistenzHelper.getInstance(this).speichereAudio(audio, notiz.getNotizId());
            // View updaten
            notiz.addAudio(audio);
            // Notiz mit Änderung speichern
            PersistenzHelper.getInstance(this).speichereNotiz(notiz);
            // Pager neu laden
            fillAudioViewPager(notiz.getAudioAnhaenge());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Berechtigungs-Check für Berechtigungen, welche diese Activity benötigt
     * (R/W External Storage, Mikrofon verwenden
     * @return true, wenn die Berechtigungen vorhanden sind, false wenn nicht
     */
    private boolean isBerechtigtFuerSpeichernUndAufnahme() {
        return BerechtigungenCheck.isBerechtigtFuerStorageLesen(this)
                && BerechtigungenCheck.isBerechtigtFuerStorageSchreiben(this)
                && BerechtigungenCheck.isBerechtigtFuerAufnahme(this);
    }


    /**
     * In der Maske zurück navigieren
     *
     * @param view aus dem GUI übergebene View
     */
    public void onZurueckNavigieren(View view) {
        finish(); // Die Anzeige kann ohne weitere Info's gelösch werden.
    }

    /**
     * Methode, um die Dot-Switches für Foto und Audio Anhänge zu initialisieren
     * @param anhaenge Liste der Anhänge
     * @param targetPager Pager, für welcher die Änderungen reagieren soll
     * @param pagerAdapter Pager-Adapter mit den Elementen
     * @param uiContainer Ziel-Container, wo die Dots gerendert werden sollen
     */
    private void initDotSwitches(@NonNull final List<?> anhaenge, @NonNull final ViewPager targetPager,
                                 @NonNull final PagerAdapter pagerAdapter,
                                 final int uiContainer) {
        if (!anhaenge.isEmpty()) {
            // Dots hinzufügen
            final TextView[] dots = new TextView[pagerAdapter.getCount()];
            final LinearLayout dotsLayout = (LinearLayout) findViewById(uiContainer);
            dotsLayout.removeAllViews();
            for (int i = 0; i < pagerAdapter.getCount(); i++) {
                dots[i] = new TextView(this);
                dots[i].setText(Html.fromHtml("&#8226;"));
                dots[i].setTextSize(30);
                dots[i].setTextColor(getResources().getColor(R.color.common_google_signin_btn_text_dark_disabled));
                dotsLayout.addView(dots[i]);
            }

            // Color auf den ersten setzen
            dots[0].setTextColor(getResources().getColor(R.color.common_google_signin_btn_text_light_focused));
            targetPager.clearOnPageChangeListeners();
            targetPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    // Do Nothing
                }

                @Override
                public void onPageSelected(int position) {
                    for (int i = 0; i < pagerAdapter.getCount(); i++) {
                        dots[i].setTextColor(getResources().getColor(R.color.common_google_signin_btn_text_dark_disabled));
                    }
                    dots[position].setTextColor(getResources().getColor(R.color.common_google_signin_btn_text_light_focused));
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                    // Do Nothing
                }
            });
        }
    }

    /**
     * Delegate-Klasse, um eine Aktion nach dem Löschen von Elementen auszuführen.
     */
    private final class DeleteDelegate implements Runnable {

        /**
         * Run-Methode, welche einen Reload startet
         */
        @Override
        public void run() {
            reload();
        }
    }
}
