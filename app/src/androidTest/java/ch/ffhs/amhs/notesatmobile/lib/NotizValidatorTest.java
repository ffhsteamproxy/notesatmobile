/*
 * Zweck:
 * Testklasse für den NotizValidator
 *
 * Author/Version/Datum:
 * mak/v1/04.06.2017 Testcases hinzugefügt
 */
package ch.ffhs.amhs.notesatmobile.lib;

import android.content.Context;
import android.support.test.InstrumentationRegistry;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ch.ffhs.amhs.notesatmobile.R;
import ch.ffhs.amhs.notesatmobile.data.Notiz;
import ch.ffhs.amhs.notesatmobile.data.NotizHandler;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

/**
 * Testklasse für den Notizvalidator
 */
public class NotizValidatorTest {

    private NotizValidator notizValidator;
    private Context context;

    @Before
    public void setUp() {
        context = InstrumentationRegistry.getTargetContext();
        notizValidator = new NotizValidator(context);
    }

    @After
    public void tearDown() {
        notizValidator = null;
    }

    /**
     * Testfall: Alles wurde gesetzt
     * Resultat: true, Fehlermeldung ist leer
     */
    @Test
    public void testValidiereNotiz_Mandatory_gesetzt() {
        final Notiz notiz = NotizHandler.erstelleNotiz();
        notiz.setTitel("Hallo Welt");
        notiz.setDetails("Hallo Welt");
        assertTrue(notizValidator.validateNotiz(notiz));
        assertEquals("Falscher LastErrorText", "", notizValidator.getLastError());
    }

    /**
     * Testfall: Titel wurde nicht gesetzt
     * Resultat: false, Fehlermeldung enthält Meldung, dass der Titel nicht gesetzt wurde
     */
    @Test
    public void testValidiereNotiz_Titel_Nicht_Gesetzt() {
        final Notiz notiz = NotizHandler.erstelleNotiz();
        notiz.setDetails("Hallo Welt");
        assertFalse(notizValidator.validateNotiz(notiz));
        assertEquals("Falscher LastErrorText", context.getString(R.string.TitelNichtGesetzt), notizValidator.getLastError());
    }

    /**
     * Testfall: Details wurden nicht gesetzt
     * Resultat: false, Fehlermeldung enthält Meldung, dass die Details nicht gesetzt wurden.
     */
    @Test
    public void testValidiereNotiz_Detail_Nicht_Gesetzt() {
        final Notiz notiz = NotizHandler.erstelleNotiz();
        notiz.setTitel("Hallo Welt");
        assertFalse(notizValidator.validateNotiz(notiz));
        assertEquals("Falscher LastErrorText", context.getString(R.string.DetailsNichtGesetzt), notizValidator.getLastError());
    }
}
