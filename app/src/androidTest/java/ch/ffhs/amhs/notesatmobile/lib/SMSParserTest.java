/*
 * Zweck:
 * Konstantenklasse für die Tabelle Notiz
 *
 * Author/Version/Datum:
 * mak/v1/15.5. Test für parseNotiz hinzugefügt
 */
package ch.ffhs.amhs.notesatmobile.lib;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ch.ffhs.amhs.notesatmobile.data.Notiz;

import static junit.framework.Assert.assertEquals;

public class SMSParserTest {
    SMSParser smsParser;

    @Before
    public void setUp() {
        smsParser = new SMSParser();
    }

    @After
    public void tearDown() {
        smsParser = null;
    }

    @Test
    public void testParseNotiz_AllGiven() {
        final String smsText = "Testnotiz;Lorem Ipsum;BlaBlabla Bla bla bla;(stichwort 1, st2, test)";
        final Notiz result = smsParser.parseNotiz(smsText);

        // Notizkopf
        assertEquals("Testnotiz", result.getTitel());
        assertEquals("BlaBlabla Bla bla bla", result.getDetails());

        // Notiz Stichworte
        assertEquals(3, result.getStichworte().size());
        assertEquals("stichwort 1", result.getStichworte().get(0));
        assertEquals("st2", result.getStichworte().get(1));
        assertEquals("test", result.getStichworte().get(2));
    }
}
