/*
 * Zweck:
 * Testklasse für den Smoketest
 *
 * Author/Version/Datum:
 * mak/v1/04.06.2017 Testcases hinzugefügt
 */
package ch.ffhs.amhs.notesatmobile;

import android.Manifest;
import android.support.test.espresso.action.ViewActions;
import android.support.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Testklasse für einen Smoke-Test
 */
public class CRUDChecklist {

    private static final String NOTIZ_TITEL = "NotizTitel";
    private static final String NOTIZ_STICHWORT = "Stichwort";
    private static final String NOTIZ_DETAIL = "NotizDetail";

    @Rule
    public ActivityTestRule<NotizenUebersichtActivity> activityRule = new ActivityTestRule<>(NotizenUebersichtActivity.class);

    @Rule
    public PermissionsRule permissionsRule = new PermissionsRule(new String[]{Manifest.permission.LOCATION_HARDWARE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,
    Manifest.permission.RECORD_AUDIO, Manifest.permission.SEND_SMS, Manifest.permission.RECEIVE_SMS});

    /**
     * Test: Erstellt eine Notiz, Zeit diese an, Bearbeitet diese und Löscht sie wieder.
     */
    @Test
    public void SmokeTest() {
        onView(withId(R.id.addNote)).perform(ViewActions.click());
        onView(withId(R.id.FeldTitelEditId)).perform(ViewActions.typeText(NOTIZ_TITEL));
        onView(withId(R.id.FeldStichworteEditId)).perform(ViewActions.typeText(NOTIZ_STICHWORT));
        onView(withId(R.id.FeldNotizEditId)).perform(ViewActions.typeText(NOTIZ_DETAIL));
        onView(withId(R.id.btnSaveNotiz)).perform(ViewActions.click());

        onView(withId(R.id.ToolbarAnzeigenTitle)).check(matches(withText(NOTIZ_TITEL)));
        onView(withId(R.id.FeldStichworteId)).check(matches(withText(NOTIZ_STICHWORT)));
        onView(withId(R.id.FeldNotizId)).check(matches(withText(NOTIZ_DETAIL)));

        onView(withId(R.id.notizBearbeiten)).perform(ViewActions.click());
        onView(withId(R.id.FeldTitelEditId)).perform(ViewActions.clearText());
        onView(withId(R.id.FeldStichworteEditId)).perform(ViewActions.clearText());
        onView(withId(R.id.FeldNotizEditId)).perform(ViewActions.clearText());

        onView(withId(R.id.FeldTitelEditId)).perform(ViewActions.typeText(NOTIZ_TITEL.concat("neu")));
        onView(withId(R.id.FeldStichworteEditId)).perform(ViewActions.typeText(NOTIZ_STICHWORT.concat("neu")));
        onView(withId(R.id.FeldNotizEditId)).perform(ViewActions.typeText(NOTIZ_DETAIL.concat("neu")));
        onView(withId(R.id.btnSaveNotiz)).perform(ViewActions.click());

        onView(withId(R.id.btnNotizLoeschen)).perform(ViewActions.click());
    }
}
